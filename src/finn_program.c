#include <stdlib.h>
#ifdef _WIN32
#include <SDL.h>
#include <zed_gl.h>
#include <nanovg.h>
#define NANOVG_GLES2_IMPLEMENTATION
#include <nanovg_gl.h>
#else
#include <SDL2/SDL.h>
#endif
#include "finn_program.h"
#include "utility/finn_error.h"
#include "utility/finn_init.h"
#include "graphics/finn_root.h"
#include "lua/finn_lua.h"
#include "input/finn_input.h"

finn_program* finn_program_create(const char* title, int32_t width, int32_t height) {
	finn_program* program = malloc(sizeof(finn_program*));
	if(program == NULL) {
		finn_error_add("Unable to allocate memory for finn_program!");
		return NULL;
	}
	if(!finn_root_init(width, height)) {
		// free(program);
		return NULL;
	}
	program->width = width;
	program->height = height;
	program->last_time = program->current_time = 0;
	program->window = SDL_CreateWindow( title, 
									    SDL_WINDOWPOS_UNDEFINED, 
										SDL_WINDOWPOS_UNDEFINED,
										width, height,
										SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	program->context = SDL_GL_CreateContext(program->window);
	return program;
}

finn_program* finn_program_create_init(const char* title, int32_t width, int32_t height, FINN_INIT_FLAGS finn_flags, uint32_t sdl_flags) {
	if(SDL_Init(sdl_flags) < 0) {
		printf("SDL Error During Initialization! %s\n", SDL_GetError());
	}
	finn_program* program = malloc(sizeof(finn_program));
	if(program == NULL) {
		finn_error_add("Unable to allocate memory for finn_program!");
		return NULL;
	}
	
	program->window = SDL_CreateWindow( title, 
									    SDL_WINDOWPOS_UNDEFINED, 
										SDL_WINDOWPOS_UNDEFINED,
										width, height,
										SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	program->context = SDL_GL_CreateContext(program->window);
	
	if(!finn_init(finn_flags)) {
		while(finn_has_error()) {
			printf("Finn Error During Initialization! %s\n", finn_error_get());
		}
	}
#ifdef _WIN32
	program->nvg_context = nvgCreateGLES2(NVG_ANTIALIAS | NVG_STENCIL_STROKES);
#endif
	if(!finn_root_init(width, height)) {
		// free(program);
		return NULL;
	}
	program->width = width;
	program->height = height;
	program->last_time = program->current_time = 0;
	return program;
}

void finn_program_destroy(finn_program* program) {
	SDL_GL_DeleteContext(program->context);
	SDL_DestroyWindow(program->window);
	free(program);
}

void finn_program_destroy_exit(finn_program* program) {
	finn_program_destroy(program);
	SDL_Quit();
	finn_quit();
}

void finn_program_update_input(finn_program* program) {
	while(SDL_PollEvent(&program->event)) {
		FINN_MOUSE_BUTTON b;
		FINN_BUTTON_PRESS p;
		switch(program->event.type)	{
			case SDL_MOUSEMOTION:
				finn_set_mouse_position(program->event.motion.x, program->event.motion.y);
				break;
			case SDL_MOUSEBUTTONUP:
			case SDL_MOUSEBUTTONDOWN:
				switch(program->event.button.button) {
					case SDL_BUTTON_LEFT: b = FINN_MOUSE_LEFT; break;
					case SDL_BUTTON_RIGHT: b = FINN_MOUSE_RIGHT; break;
					case SDL_BUTTON_MIDDLE: b = FINN_MOUSE_MIDDLE; break;
				}
				switch(program->event.button.state) {
					case SDL_PRESSED: p = FINN_PRESS_DOWN; break;
					case SDL_RELEASED: p = FINN_PRESS_UP; break;
				}
				finn_set_mouse_button(b, p);
				break;
			case SDL_KEYUP:
			case SDL_KEYDOWN:
				finn_set_key_press(SDL_GetKeyName(program->event.key.keysym.sym),
					program->event.key.keysym.mod == KMOD_SHIFT ?
						FINN_MOD_SHIFT :
						program->event.key.keysym.mod == KMOD_CTRL ?
							FINN_MOD_CONTROL :
							program->event.key.keysym.mod == KMOD_ALT ?
								FINN_MOD_ALT :
								FINN_MOD_NONE,
					program->event.type == SDL_KEYDOWN ? FINN_PRESS_DOWN : FINN_PRESS_UP
				);
				break;
			case SDL_QUIT:
				finn_set_quit_event();
				break;
		}
	}
}

void finn_program_clear_background() {
//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//	glEnable(GL_BLEND);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//	finn_root_draw();
}

void finn_program_swap_buffer(finn_program* program) {
	finn_lua_update(program->delta_time);
	
	SDL_GL_SwapWindow(program->window);
	
	int tempTime = SDL_GetTicks() - program->last_time;
	if(tempTime < g_root->norm_fps) {
		SDL_Delay(g_root->norm_fps - tempTime);
	}
	
	program->current_time = SDL_GetTicks();
	program->delta_time = program->current_time - program->last_time;
	program->last_time = program->current_time;
	
	finn_input_new_frame();
	finn_program_update_input(program);
}

void finn_program_update(finn_program* program) {
	program->current_time = SDL_GetTicks();
	program->delta_time = program->current_time - program->last_time;
	program->last_time = program->current_time;
	
	finn_input_new_frame();
	finn_program_update_input(program);
}
