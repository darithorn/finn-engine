#include <stdlib.h>
#include "view/finn_camera.h"
#include "math/finn_mat4.h"
#include "utility/finn_error.h"

//finn_camera *g_finn_main_camera;

finn_camera *finn_camera_create_ortho(float width, float height, float near, float far) {
	finn_camera *camera = malloc(sizeof(finn_camera));
	if(camera == NULL) {
		finn_error_add("Could not allocate memory for finn_camera!");
		return NULL;
	}
	camera->type = FINN_PROJECTION_ORTHOGRAPHIC;
	finn_mat4_ortho(&camera->projection[0], 0, width, height, 0, near, far);
	finn_mat4_identity(&camera->view[0]);
	// if(g_finn_main_camera == NULL) {
	// 	g_finn_main_camera = camera;
	// }
	return camera;
}

void finn_camera_destroy(finn_camera *camera) {
	free(camera);
}
