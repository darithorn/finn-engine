#include <stdlib.h>
#include <stdio.h>
#include "graphics/finn_texture.h"
#include "utility/finn_error.h"

// TODO(darithorn): Add #if's to designate usage of cairo or a different drawing backend
int allocate_data(finn_texture* t) {
	t->data = calloc(4 * t->width * t->height, sizeof(unsigned char));
	if(t->data == NULL) {
		finn_error_add("Failed to allocate memory for t->data!");
		return -1;
	}
	#ifdef __LIBCAIRO__
	int stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, t->width);
	t->surface = cairo_image_surface_create_for_data(t->data, 
													 CAIRO_FORMAT_ARGB32, 
													 t->width, t->height, 
													 stride);
	if(cairo_surface_status(t->surface) != CAIRO_STATUS_SUCCESS) {
		// TODO: free(t->data);
		finn_error_add("Failed to create cairo_surface_t!");
		return -1;
	}
	
	t->context = cairo_create(t->surface);
	if(cairo_status(t->context) != CAIRO_STATUS_SUCCESS) {
		// TODO: free(t->data);
		finn_error_add("Failed to create cairo_t!");
		return -1;
	}
	#endif
	return 1;
}

finn_texture* finn_texture_create(float width, float height) {
	finn_texture* t = malloc(sizeof(finn_texture));
	if(t == NULL) {
		finn_error_add("Failed to allocate memory for finn_texture!");
		return NULL;
	}
	t->width = width;
	t->height = height;
#ifdef __LIBCAIRO__
	t->context = NULL;
	t->surface = NULL;
#endif
	t->data = NULL;
	if(!finn_texture_resize(t, width, height)) {
		free(t);
		return NULL;
	}
	
	glGenTextures(1, &t->id);
	finn_texture_bind(t);
#ifdef __LIBCAIRO__
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
#endif
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	return t;
}

void finn_texture_destroy(finn_texture* texture) {
	#ifdef __LIBCAIRO__
	cairo_destroy(texture->context);
	cairo_surface_destroy(texture->surface);
	#endif
	free(texture->data);
	glDeleteTextures(1, &texture->id);
	free(texture);
}

int finn_texture_resize(finn_texture* texture, float width, float height) {
	free(texture->data);
	texture->width = width;
	texture->height = height;
	return allocate_data(texture);
}

void finn_texture_bind(finn_texture* texture) {
	if(texture == NULL) {
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	} else {
#ifdef __LIBCAIRO__
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture->id);
		glActiveTexture(GL_TEXTURE0);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RGBA,
			texture->width,
			texture->height,
			0,
			GL_BGRA,
			GL_UNSIGNED_BYTE,
			texture->data
		);
#endif
	}
}
