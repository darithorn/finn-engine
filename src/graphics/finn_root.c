#include <stdlib.h>
#include "graphics/finn_root.h"
#include "graphics/finn_draw_utility.h"
#include "utility/finn_error.h"

int finn_root_init(uint32_t width, uint32_t height) {
    g_root = malloc(sizeof(finn_root));
    if(g_root == NULL) {
        finn_error_add("Unable to allocate memory for g_root!");
        return 0;
    }
    g_root->width = width;
    g_root->height = height;
    g_root->r = g_root->g = g_root->b = 0;
	g_root->pixel_x_ratio = 2.0f / width;
	g_root->pixel_y_ratio = 2.0f / height;
	finn_root_limit_fps(60);
    // g_root->quad = finn_quad_create(width, height, width, height);
    // g_root->texture = finn_texture_create(width, height);
    if(finn_has_error()) {
        return 0;
    }
    // finn_texture_fill_background_normalized_rgb(g_root->texture, 0, 0, 0);
    return 1;
}

void finn_root_destroy() {
    // finn_quad_destroy(g_root->quad);
    // finn_texture_destroy(g_root->texture);
    free(g_root);
}

void finn_root_draw() {
    // finn_texture_bind(g_root->texture);
    // finn_quad_draw(g_root->quad, 0, 0, GL_2_X, 1);
    // finn_texture_bind(NULL);
}

void finn_root_limit_fps(int fps) {
	g_root->fps = fps;
	g_root->norm_fps = 1000 / fps;
}

float finn_root_get_width() {
    return g_root->width;
}

float finn_root_get_height() {
    return g_root->height;
}
