#include <stdlib.h>
#include "graphics/finn_font.h"
#include "utility/finn_error.h"

finn_font* finn_font_create(const char* face, double size, FINN_FONT_SLANT slant, FINN_FONT_WEIGHT weight) {
	finn_font* font = malloc(sizeof(finn_font));
	if(font == NULL) {
		finn_error_add("Unable to allocate memory for finn_font!");
		return NULL;
	}
	font->face = face;
	font->size = size;
	#ifdef __LIBCAIRO__
	switch(slant) {
		case FINN_SLANT_NORMAL:
			font->slant = CAIRO_FONT_SLANT_NORMAL;
			break;
		case FINN_SLANT_ITALIC:
			font->slant = CAIRO_FONT_SLANT_ITALIC;
			break;
		case FINN_SLANT_OBLIQUE:
			font->slant = CAIRO_FONT_SLANT_OBLIQUE;
			break;
	}
	font->weight = weight == FINN_WEIGHT_NORMAL ? CAIRO_FONT_WEIGHT_NORMAL : CAIRO_FONT_WEIGHT_BOLD;
	#endif
	return font;
}

void finn_font_destroy(finn_font* font) {
	free(font);
}
