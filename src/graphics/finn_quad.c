#include <stdlib.h>
#include "graphics/finn_quad.h"
#include "graphics/finn_root.h"
#include "utility/finn_error.h"

finn_quad* finn_quad_create(uint32_t root_width, uint32_t root_height, uint32_t width, uint32_t height) {
	finn_quad* q = malloc(sizeof(finn_quad));
	if(q == NULL) {
		finn_error_add("Unable to create finn_quad!");
		return NULL;
	}
	q->x_offset = q->y_offset  = 0;
	q->width = width;
	q->height = height;
	
	float screen_coord_width, screen_coord_height;
	
	if(root_width == width) {
		screen_coord_width = 2;
	} else {
		screen_coord_width = (width / (root_width / 2.0f));
	}
	
	if(root_height == height) {
		screen_coord_height = 2;
	} else {
		screen_coord_height = (height / (root_height / 2.0f));
	}
	
	finn_quad_set_corner_position(q, BOTTOM_LEFT, 0, -screen_coord_height);
	finn_quad_set_corner_position(q, BOTTOM_RIGHT, screen_coord_width, -screen_coord_height);
	finn_quad_set_corner_position(q, TOP_LEFT, 0, 0);
	finn_quad_set_corner_position(q, TOP_RIGHT, screen_coord_width, 0);
	
	finn_quad_set_corner_tex_coord(q, BOTTOM_LEFT, 0, 1);
	finn_quad_set_corner_tex_coord(q, BOTTOM_RIGHT, 1, 1);
	finn_quad_set_corner_tex_coord(q, TOP_LEFT, 0, 0);
	finn_quad_set_corner_tex_coord(q, TOP_RIGHT, 1, 0);
	
	return q;
}

void finn_quad_destroy(finn_quad* quad) {
	free(quad);
}

// TODO(darithorn): add support for OpenGL 2.x and Opengl 3.x
void finn_quad_draw(finn_quad* quad, float x, float y, GL_VERSION_X gl_version, int is_root) {
	finn_quad_move_by_pixel(quad, g_root->pixel_x_ratio, g_root->pixel_y_ratio, x, y);
	switch(gl_version) {
		case GL_3_X:
		case GL_2_X:
		default:
#ifdef __IMM_MODE_SUPP__
			glLoadIdentity();
			glPushMatrix();
			glTranslatef(quad->x_offset - 1.0f, quad->y_offset + 1.0f, 0.0f);
			glBegin(GL_QUADS);
				// Bottom Left
				glTexCoord2f(quad->tex_coords[0], quad->tex_coords[1]);
				glVertex2f(quad->positions[0], quad->positions[1]);
				// Top Left
				glTexCoord2f(quad->tex_coords[2], quad->tex_coords[3]);
				glVertex2f(quad->positions[2], quad->positions[3]);
				// Top Right
				glTexCoord2f(quad->tex_coords[4], quad->tex_coords[5]);
				glVertex2f(quad->positions[4], quad->positions[5]);
				// Bottom Right
				glTexCoord2f(quad->tex_coords[6], quad->tex_coords[7]);
				glVertex2f(quad->positions[6], quad->positions[7]);
			glEnd();
			glPopMatrix();
#else
			finn_error_add("No backend available! Immediate mode is not supported.");
#endif
			break;
	}
}

int finn_quad_set_corner_position(finn_quad* quad, QUAD_CORNER corner, GLfloat x, GLfloat y) {
	int offset;
	switch(corner) {
		case BOTTOM_LEFT:
			offset = 0;
			break;
		case BOTTOM_RIGHT:
			offset = 6;
			break;
		case TOP_LEFT:
			offset = 2;
			break;
		case TOP_RIGHT:
			offset = 4;
			break;
		default:
			finn_error_add("Expected proper QUAD_CORNER!");
			return 0;
	}
	quad->positions[offset] = x;
	quad->positions[offset + 1] = y;
	return 1;
}

int finn_quad_set_corner_tex_coord(finn_quad* quad, QUAD_CORNER corner, GLfloat u, GLfloat v) {
	int offset;
	switch(corner) {
		case BOTTOM_LEFT:
			offset = 0;
			break;
		case BOTTOM_RIGHT:
			offset = 6;
			break;
		case TOP_LEFT:
			offset = 2;
			break;
		case TOP_RIGHT:
			offset = 4;
			break;
		default:
			finn_error_add("Expected proper QUAD_CORNER!");
			return 0;
	}
	quad->tex_coords[offset] = u;
	quad->tex_coords[offset + 1] = v;
	return 1;
}

int finn_quad_resize(finn_quad* quad, uint32_t width, uint32_t height) {
	float g_width = width / (g_root->width / 2.0f);
	float g_height = height / (g_root->height / 2.0f);
	
	if(!finn_quad_set_corner_position(quad, BOTTOM_LEFT, 0, -g_height)) return 0;
	if(!finn_quad_set_corner_position(quad, BOTTOM_RIGHT, g_width, -g_height)) return 0;
	// Not needed, it's already (0, 0)
//	finn_setQuadCornerPosition(quad, TOP_LEFT, 0, 0);
	if(!finn_quad_set_corner_position(quad, TOP_RIGHT, g_width, 0)) return 0;
	return 1;
}

void finn_quad_move_by_pixel(finn_quad* quad, float pixel_x, float pixel_y, int pixel_x_amount, int pixel_y_amount) {
	quad->x_offset = pixel_x * pixel_x_amount;
	quad->y_offset = pixel_y * pixel_y_amount;
}	
