#include <stdlib.h>
#ifdef __LIBCAIRO__
#include <cairo/cairo.h>
#endif
#include "graphics/finn_sprite.h"
#include "graphics/finn_root.h"
#include "graphics/finn_draw_utility.h"
#include "utility/finn_error.h"

// TODO(darithorn): Add #if's to designate usage of cairo or a different drawing backend
// TODO(darithorn): Add single finn_sprite_create function that infers file type
finn_sprite* finn_sprite_create_from_svg(const char* file) {
	finn_image* img = finn_image_create_from_svg(file);
	return finn_sprite_create_from_image(img);
}

finn_sprite* finn_sprite_create_from_png(const char* file) {
	finn_image* img = finn_image_create_from_png(file);
	return finn_sprite_create_from_image(img);
}

finn_sprite* finn_sprite_create_from_image(finn_image* img) {
	finn_sprite* sprite = malloc(sizeof(finn_sprite));
	if(sprite == NULL) {
		finn_error_add("Unable to allocate memory for finn_sprite!");
		return NULL;
	}
	sprite->img = img;
	sprite->texture = finn_texture_create(sprite->img->width, sprite->img->height);
	sprite->quad = finn_quad_create(finn_root_get_width(), finn_root_get_height(), sprite->img->width, sprite->img->height);
	#ifdef __LIBCAIRO__
	cairo_matrix_init_scale(&sprite->scale, 1, 1);
	cairo_set_matrix(sprite->texture->context, &sprite->scale);
	#endif
	finn_texture_draw_image(sprite->texture, sprite->img, 0, 0);
	if(finn_has_error()) {
		// free(sprite);
		return NULL;
	}
	return sprite;
}

void finn_sprite_destroy(finn_sprite* sprite) {
	finn_texture_destroy(sprite->texture);
	finn_quad_destroy(sprite->quad);
	finn_image_destroy(sprite->img);
	free(sprite);
}

void finn_sprite_draw(finn_sprite* sprite, float x, float y) {
	finn_texture_bind(sprite->texture);
	finn_quad_draw(sprite->quad, x, y * -1, GL_2_X, 0);
	finn_texture_bind(NULL);
}

void finn_sprite_resize(finn_sprite* sprite, float w, float h) {
	finn_quad_resize(sprite->quad, w, h);
	
	// Refresh Image if SVG
	if(sprite->img->image_type == IMAGE_TYPE_SVG) {
		finn_texture_resize(sprite->texture, w, h);
		
		float x_scale = w / (float)sprite->img->width;
		float y_scale = h / (float)sprite->img->height;
		#ifdef __LIBCAIRO__
		cairo_matrix_init_scale(&sprite->scale, x_scale, y_scale);
		cairo_set_matrix(sprite->texture->context, &sprite->scale);
		#endif
		finn_texture_draw_image(sprite->texture, sprite->img, 0, 0);
	}
}

void finn_sprite_resize_aspect_ratio(finn_sprite* sprite, float scale) {
	int new_width = sprite->img->width * scale;
	int new_height = sprite->img->height * scale;
	
	finn_texture_resize(sprite->texture, new_width, new_height);
	finn_quad_resize(sprite->quad, new_width, new_height);
	
	#ifdef __LIBCAIRO__
	cairo_matrix_init_scale(&sprite->scale, scale, scale);
	cairo_set_matrix(sprite->texture->context, &sprite->scale);
	#endif

	finn_texture_draw_image(sprite->texture, sprite->img, 0, 0);
}

void finn_sprite_set_region(finn_sprite* sprite, float x, float y, float w, float h) {
	finn_sprite_resize(sprite, w, h);
	
	float x_norm = x / sprite->img->width;
	float y_norm = y / sprite->img->height;
	float w_norm = w / sprite->img->width;
	float h_norm = h / sprite->img->height;
	
	finn_quad_set_corner_tex_coord(sprite->quad, BOTTOM_LEFT, x_norm, y_norm + h_norm);
	finn_quad_set_corner_tex_coord(sprite->quad, BOTTOM_RIGHT, x_norm + w_norm, y_norm + h_norm);
	finn_quad_set_corner_tex_coord(sprite->quad, TOP_LEFT, x_norm, y_norm);
	finn_quad_set_corner_tex_coord(sprite->quad, TOP_RIGHT, x_norm + w_norm, y_norm);
}
