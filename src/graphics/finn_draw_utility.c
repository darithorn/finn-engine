#ifdef __LIBCAIRO__
#include <cairo/cairo.h>
#endif
#ifdef __LIBRSVG__
#include <glib.h>
#include <librsvg-2.0/librsvg/rsvg.h>
#endif
#ifdef _WIN32
#include <zed_gl.h>
#else
#include <GL/gl.h>
#endif
#include "graphics/finn_draw_utility.h"
#include "graphics/finn_root.h"
#include "utility/finn_error.h"

// TODO(darithorn): Add #if's to designate usage of cairo or a different drawing backend
void finn_texture_set_rgb(finn_texture* texture, float r, float g, float b) {
	r /= 255.0f;
	g /= 255.0f;
	b /= 255.0f;
	#ifdef __LIBCAIRO__
	cairo_set_source_rgb(texture->context, r, g, b);
	#endif
}

void finn_texture_set_rgba(finn_texture* texture, float r, float g, float b, float a) {
	r /= 255.0f;
	g /= 255.0f;
	b /= 255.0f;
	#ifdef __LIBCAIRO__	
	cairo_set_source_rgba(texture->context, r, g, b, a);
	#endif
}

void finn_texture_set_position(finn_texture* texture, float x, float y) {
	#ifdef __LIBCAIRO__
	cairo_move_to(texture->context, x, y);
	#endif
}

void finn_texture_fill_background_color(finn_texture* texture, finn_color color) {
	#ifdef __LIBCAIRO__
	finn_texture_fill_background_normalized_rgb(texture, color.r, color.g, color.b);
	#endif
}

void finn_texture_fill_background_rgb(finn_texture* texture, float r, float g, float b) {
	r /= 255.0f;
	g /= 255.0f;
	b /= 255.0f;
	#ifdef __LIBCAIRO__
	cairo_set_source_rgb(texture->context, r, g, b);
	cairo_rectangle(texture->context, 0, 0, texture->width, texture->height);
	cairo_fill(texture->context);
	#endif
}

void finn_texture_fill_background_normalized_rgb(finn_texture* texture, float r, float g, float b) {
	#ifdef __LIBCAIRO__
	cairo_set_source_rgb(texture->context, r, g, b);
	cairo_rectangle(texture->context, 0, 0, texture->width, texture->height);
	cairo_fill(texture->context);
	#endif
}

void finn_texture_draw_image(finn_texture* texture, finn_image* image, float x, float y) {
	switch(image->image_type) {
		case IMAGE_TYPE_SVG:
			#ifdef __LIBCAIRO__
			if(!rsvg_handle_render_cairo(image->handle, texture->context)) {
				finn_error_add("Something went wrong with drawing!\n");
			}
			#endif
			break;
		case IMAGE_TYPE_PNG:
			#ifdef __LIBCAIRO__
			cairo_set_source_surface(texture->context, image->handle, x, y);
			cairo_paint(texture->context);
			#endif
			break;
		case IMAGE_TYPE_JPG:
			finn_error_add("JPG is not supported!");
			break;
	}
}

// TODO(darithorn): Add proper text support
void finn_root_set_font(finn_font* font) {
	#ifdef __LIBCAIRO__
	// cairo_select_font_face(g_root->texture->context, font->face, font->slant, font->weight);
	// cairo_set_font_size(g_root->texture->context, font->size);
	#endif
}

void finn_root_set_text_rgb(float r, float g, float b) {
	r /= 255.0f;
	g /= 255.0f;
	b /= 255.0f;
	#ifdef __LIBCAIRO__
	// cairo_set_source_rgb(g_root->texture->context, r, g, b);
	#endif
}

void finn_root_draw_text_with_font(finn_font* font, const char* text, double x, double y) {
	finn_root_set_font(font);
	#ifdef __LIBCAIRO__
	// cairo_move_to(g_root->texture->context, x, g_root->height - y);
	// cairo_show_text(g_root->texture->context, text);
	#endif
}

void finn_root_draw_text(const char* text, double x, double y) {
	#ifdef __LIBCAIRO__
	// cairo_move_to(g_root->texture->context, x, g_root->height - y);
	// cairo_show_text(g_root->texture->context, text);
	#endif
}
