#include "graphics/finn_color.h"

// TODO(darithorn): designate out parameters
void finn_color_normalized(finn_color color, float* red, float* green, float* blue) {
	*red 	= color.r / 255.0f;
	*green 	= color.g / 255.0f;
	*blue 	= color.b / 255.0f;
}
