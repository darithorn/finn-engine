#include <stdlib.h>
#ifdef __LIBCAIRO__
#include <cairo/cairo.h>
#endif
#include "graphics/finn_image.h"
#include "utility/finn_error.h"

finn_image* finn_image_create_from_svg(const char* filename) {
	finn_image* img = malloc(sizeof(finn_image));
	if(img == NULL) {
		finn_error_add("Unable to allocate memory for finn_image!");
		return NULL;
	}
	img->file = filename;
	img->image_type = IMAGE_TYPE_SVG;
	#ifdef __LIBRSVG__
	GError* g_error;
	img->handle = rsvg_handle_new_from_file(filename, &g_error);
	if(img->handle == NULL) {
		finn_error_add("There was an error with loading the file!");
		return NULL;
	}
	RsvgDimensionData data;
	rsvg_handle_get_dimensions(img->handle, &data);
	img->width = data.width;
	img->height = data.height;
	#endif
	return img;
}

finn_image* finn_image_create_from_png(const char* filename) {
	finn_image* img = malloc(sizeof(finn_image));
	if(img == NULL) {
		finn_error_add("Unable to allocate memory for finn_image!");
		return NULL;
	}
	img->file = filename;
	img->image_type = IMAGE_TYPE_PNG;
	#ifdef __LIBCAIRO__
	img->handle = cairo_image_surface_create_from_png(filename);
	if(cairo_surface_status(img->handle) != CAIRO_STATUS_SUCCESS) {
		// TODO: free(img);
		finn_error_add(cairo_status_to_string(cairo_surface_status(img->handle)));
		return NULL;
	}
	img->width = cairo_image_surface_get_width(img->handle);
	img->height = cairo_image_surface_get_height(img->handle);
	#endif
	return img;
}

void finn_image_destroy(finn_image* img) {
	switch(img->image_type) {
		case IMAGE_TYPE_SVG:
			#ifdef __LIBRSVG__
			g_object_unref(img->handle);
			#endif
			free(img);
			break;
		case IMAGE_TYPE_PNG:
			#ifdef __LIBCAIRO__	
			cairo_surface_destroy(img->handle);
			#endif
			free(img);
			break;
		default:
			finn_error_add("Handling image types other than SVG is not supported!");
			break;
	}
}
