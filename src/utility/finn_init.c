#ifdef __LIBRSVG__
#include <glib-2.0/glib-object.h>
#endif
#ifdef _WIN32
#define ZED_GL_IMPLEMENTATION
#include <zed_gl.h>
#undef ZED_GL_IMPLEMENTATION
#endif
#include "utility/finn_init.h"
#include "utility/finn_error.h"
#include "graphics/finn_root.h"
#include "input/finn_input.h"
#include "lua/finn_lua.h"

int finn_init(FINN_INIT_FLAGS flags) {
	switch(flags) {
		case FINN_INIT_EVERYTHING:
			if(!finn_errors_init()) {
				return 0;
			}
			if(!finn_input_init()) {
				return 0;
			}
#ifdef _WIN32
			zed_gl_load_all();
#endif
#ifdef __LIBRSVG__
#if !(GLIB_CHECK_VERSION(2, 36, 0))
			g_type_init();
#endif
#endif
			break;
	}
	return 1;
}

void finn_quit() {
	finn_lua_cleanup();
	finn_root_destroy();
	finn_input_free();
	finn_errors_free();
}
