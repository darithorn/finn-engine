#include <stdlib.h>
#include "utility/finn_error.h"

typedef struct {
	int number;
	const char** messages;
} finn_errors;
// messages goes least recent -> most recent

finn_errors* errors;

int finn_errors_init() {
	errors = malloc(sizeof(finn_errors));
	if(errors == NULL) {
		return 0;
	}
	errors->number = 0;
	errors->messages = NULL;
	return 1;
}

void finn_errors_free() {
	free(errors);
}

int finn_has_error() {
	return errors->number;
}

const char* finn_error_get() {
	if(errors->number == 0) {
		return "No errors!";
	}
	const char* message = errors->messages[errors->number - 1];
	errors->number -= 1;
	// realloc automatically frees the message
	errors->messages = realloc(errors->messages, sizeof(typeof(errors->messages)) * errors->number);
	return message;
}

void finn_error_add(const char* message) {
	errors->number += 1;
	errors->messages = realloc(errors->messages, sizeof(typeof(errors->messages)) * errors->number);
	errors->messages[errors->number - 1] = message;
}
