#include <stdlib.h>
#include "utility/finn_rect.h"

float* finn_rect_get_corners(finn_rect rect) {
	// TODO(darithorn): use a float[8] out parameter
	float* corners = calloc(8, sizeof(uint32_t));
	corners[0] = rect.x;
	corners[1] = rect.y + rect.height;
	corners[2] = rect.x;
	corners[3] = rect.y;
	corners[4] = rect.x + rect.width;
	corners[5] = rect.y;
	corners[6] = rect.x + rect.width;
	corners[7] = rect.y + rect.height;
	return corners;
}
