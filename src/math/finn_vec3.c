#include <string.h>
#include <math.h>
#include <stdio.h>
#include "math/finn_vec3.h"

void finn_vec3_print(float *vec) {
	printf("[x: %f\ty: %f\tz: %f]\n", vec[0], vec[1], vec[2]);
}

void finn_vec3_up(float *vec) {
	vec[0] = 0;
	vec[1] = 1;
	vec[2] = 0;
}

void finn_vec3_right(float *vec) {
	vec[0] = 1;
	vec[1] = 0;
	vec[2] = 0;
}

void finn_vec3_forward(float *vec) {
	vec[0] = 0;
	vec[1] = 0;
	vec[2] = 1;
}

void finn_vec3_zero(float *vec) {
	memset(vec, 0, sizeof(float) * 3);
}

void finn_vec3_one(float *vec) {
	memset(vec, 1, sizeof(float) * 3);
}


float finn_vec3_dot(float *lhs, float *rhs) {
	return (lhs[0] * rhs[0]) + (lhs[1] * rhs[1]) + (lhs[2] * rhs[2]);
}

float finn_vec3_mag(float *lhs) {
	return sqrt(finn_vec3_dot(lhs, lhs));
}

void finn_vec3_normalize(float *result, float *vec) {
	float mag = finn_vec3_mag(vec);
	float inv = 1.0f / mag;
	result[0] = vec[0] * inv;
	result[1] = vec[1] * inv;
	result[2] = vec[2] * inv;
}

void finn_vec3_cross(float *result, float *lhs, float *rhs) {
	result[0] = (lhs[1] * rhs[2]) - (lhs[2] * rhs[1]);
	result[1] = (lhs[2] * rhs[0]) - (lhs[0] * rhs[2]);
	result[2] = (lhs[0] * rhs[1]) - (lhs[1] * rhs[0]);
}

void finn_vec3_mul_vec3(float *result, float *lhs, float *rhs) {
	result[0] = lhs[0] * rhs[0];
	result[1] = lhs[1] * rhs[1];
	result[2] = lhs[2] * rhs[2];
}

void finn_vec3_mul_scalar(float *result, float *lhs, float scalar) {
	result[0] = lhs[0] * scalar;
	result[1] = lhs[1] * scalar;
	result[2] = lhs[2] * scalar;
}

void finn_vec3_sub_vec3(float *result, float *lhs, float *rhs) {
	result[0] = lhs[0] - rhs[0];
	result[1] = lhs[1] - rhs[1];
	result[2] = lhs[2] - rhs[2];
}
