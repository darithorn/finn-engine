#include "math/finn_vec4.h"

void finn_vec4_mul_vec4(float *result, float *lhs, float *rhs) {
	result[0] = lhs[0] * rhs[0];
	result[1] = lhs[1] * rhs[1];
	result[2] = lhs[2] * rhs[2];
	result[3] = lhs[3] * rhs[3];
}

void finn_vec4_mul_mat4(float *result, float *vec, float *mat) {
	result[0] = (vec[0] * mat[0]) + (vec[1] * mat[4]) + (vec[2] * mat[8]) + (vec[3] * mat[12]);
	result[1] = (vec[0] * mat[1]) + (vec[1] * mat[5]) + (vec[2] * mat[9]) + (vec[3] * mat[13]);
	result[2] = (vec[0] * mat[2]) + (vec[1] * mat[6]) + (vec[2] * mat[10]) + (vec[3] * mat[14]);
	result[3] = (vec[0] * mat[3]) + (vec[1] * mat[7]) + (vec[2] * mat[11]) + (vec[3] * mat[15]);
}
