#include <string.h>
#include <math.h>
#include <stdio.h>
#include "math/finn_mat4.h"
#include "math/finn_vec4.h"
#include "math/finn_vec3.h"

void finn_mat4_identity(float *mat) {
	memset(mat, 0, sizeof(float) * 16);
	mat[0] = mat[5] = mat[10] = mat[15] = 1;
}

void finn_mat4_translate(float *mat, float *vec) {
	finn_mat4_identity(mat);
	mat[12] = vec[0];
	mat[13] = vec[1];
	mat[14] = vec[2];
}

void finn_mat4_translate_xyz(float *mat, float x, float y, float z) {
	float vec[3] = { x, y, z };
	finn_mat4_translate(mat, vec);
}

void finn_mat4_scale(float *mat, float *vec) {
	memset(mat, 0, sizeof(float) * 16);
	mat[0] = vec[0];
	mat[5] = vec[1];
	mat[10] = vec[2];
	mat[15] = 1.0f;
}

void finn_mat4_scale_xyz(float *mat, float x, float y, float z) {
	float vec[3] = { x, y, z };
	finn_mat4_scale(mat, vec);
}

void finn_mat4_rotate(float *mat, float *axis, float rad) {
	float s = sin(rad);
	float c = cos(rad);
	float t = 1 - c;

	finn_vec3_normalize(axis, axis);

	memset(mat, 0, sizeof(float) * 16);
	mat[0] = axis[0] * axis[0] * t + c;
	mat[1] = axis[1] * axis[0] * t + axis[2] * s;
	mat[2] = axis[2] * axis[0] * t - axis[1] * s;

	mat[4] = axis[0] * axis[1] * t - axis[2] * s;
	mat[5] = axis[1] * axis[1] * t + c;
	mat[6] = axis[2] * axis[1] *t + axis[0] * s;

	mat[8] = axis[0] * axis[2] * t + axis[1] * s;
	mat[9] = axis[1] * axis[2] * t - axis[0] * s;
	mat[10] = axis[2] * axis[2] * t + c;
	mat[15] = 1.0f;
}

void finn_mat4_ortho(float *mat, float left, float right, float bottom, float top, float near, float far) {
	float lr = 1 / (left - right);
	float bt = 1 / (bottom - top);
	float nf = 1 / (near - far);
	memset(mat, 0, sizeof(float) * 16);
	mat[0] = -2.0f * lr;
	mat[5] = -2.0f * bt;
	mat[10] = 2.0f * nf;
	mat[12] = (left + right) * lr;
	mat[13] = (top + bottom) * bt;
	mat[14] = (far + near) * nf;
	mat[15] = 1.0f;
}

void finn_mat4_look_at(float *result, float *eye, float *at, float *up) {
	float temp[3];
	float f[3];
	finn_vec3_sub_vec3(temp, at, eye);
	finn_vec3_normalize(f, temp);
	float s[3];
	finn_vec3_cross(temp, f, up);
	finn_vec3_normalize(s, temp);		
	float u[3];
	finn_vec3_cross(u, s, f);

	float s_eye_dot = finn_vec3_dot(s, eye);
	float u_eye_dot = finn_vec3_dot(u, eye);
	float f_eye_dot = finn_vec3_dot(f, eye);
	
	finn_mat4_identity(result);
	result[0] = s[0];
	result[1] = s[1];
	result[2] = s[2];
	
	result[4] = u[0];
	result[5] = u[1];
	result[6] = u[2];
	
	result[8] = -f[0];
	result[9] = -f[1];
	result[10] = -f[2];
	
	result[12] = s_eye_dot;
	result[13] = u_eye_dot;
	result[14] = f_eye_dot;
	result[15] = 1.0f;
}

void finn_mat4_perspective(float *result, float fov, float aspect, float near, float far) {
	const float tanHalfFov = tan(fov / 2.0f);
	memset(result, 0, sizeof(float) * 16);
	result[0] = 1.0f / (aspect * tanHalfFov);
	result[5] = 1.0f / (tanHalfFov);
	result[10] = - (far + near) / (far - near);
	result[11] = - 1.0f;
	result[14] = - (2.0f * far * near) / (far - near);
}

void finn_mat4_mul_mat4(float *result, float *lhs, float *rhs) {
	finn_vec4_mul_mat4(&result[0], &lhs[0], rhs);
	finn_vec4_mul_mat4(&result[4], &lhs[4], rhs);
	finn_vec4_mul_mat4(&result[8], &lhs[8], rhs);
	finn_vec4_mul_mat4(&result[12], &lhs[12], rhs);
}
