#include <string.h>
#include <math.h>
#include "math/finn_vec2.h"

void finn_vec2_one(float *result) {
	memset(result, 1, sizeof(float) * 2);
}

void finn_vec2_zero(float *result) {
	memset(result, 0, sizeof(float) * 2);
}

void finn_vec2_up(float *result) {
	result[0] = 0;
	result[1] = 1;
}

void finn_vec2_right(float *result) {
	result[0] = 1;
	result[1] = 0;
}

void finn_vec2_create(float *result, float x, float y) {
	result[0] = x;
	result[1] = y;
}

float finn_vec2_dot(float *lhs, float *rhs) {
	return (lhs[0] * rhs[0]) + (lhs[1] * rhs[1]);
}

float finn_vec2_magnitude(float *vec) {
	float temp = finn_vec2_dot(vec, vec);
	return sqrt(temp * temp);
}

void finn_vec2_normalize(float *result, float *vec) {
	float mag = finn_vec2_magnitude(vec);
	result[0] = vec[0] / mag;
	result[1] = vec[1] / mag;
}

void finn_vec2_add_vec2(float *result, float *lhs, float *rhs) {
	result[0] = lhs[0] + rhs[0];
	result[1] = lhs[1] + rhs[1];
}

void finn_vec2_sub_vec2(float *result, float *lhs, float *rhs) {
	result[0] = lhs[0] - rhs[0];
	result[1] = lhs[1] - rhs[1];
}

void finn_vec2_mul(float *result, float *lhs, float scalar) {
	result[0] = lhs[0] * scalar;
	result[1] = lhs[1] * scalar;
}
