#include <stdlib.h>
#include "gui/finn_gui_manager.h"
#include "gui/finn_gui_item.h"
#include "utility/finn_error.h"

finn_gui_manager* g_gui_manager;

int finn_gui_manager_step() {
	g_gui_manager->gui_items_max += FINN_GUI_ITEM_STEP;
	g_gui_manager->gui_items = realloc(g_gui_manager->gui_items, sizeof(finn_gui_item*) * g_gui_manager->gui_items_max);
	if(g_gui_manager->gui_items == NULL) {
		finn_error_add("There was an error reallocating guiItems!");
		return -1;
	}
}

int finn_gui_manager_init() {
	g_gui_manager = malloc(sizeof(finn_gui_manager));
	if(g_gui_manager == NULL) {
		finn_error_add("There was an error initializing finn_guiManager!");
		return -1;
	}
	g_gui_manager->focus_item = NULL;
	g_gui_manager->gui_items = calloc(FINN_GUI_ITEM_STEP, sizeof(finn_gui_item*));
	g_gui_manager->gui_items_length = 0;
	g_gui_manager->gui_items_max = FINN_GUI_ITEM_STEP;
	return 0;
}

void finn_gui_manager_free() {
	free(g_gui_manager->gui_items);
	free(g_gui_manager);
}

int finn_gui_manager_add_item(finn_gui_item* item) {
	if(g_gui_manager->gui_items_length + 1 >= g_gui_manager->gui_items_max) {
		if(finn_gui_manager_step() < 0) {
			// There was en error.
			return -1;
		}
	}
	g_gui_manager->gui_items[g_gui_manager->gui_items_length - 1] = item;
	g_gui_manager->gui_items_length += 1;
	return 0;
}
