#include <stdlib.h>
#include "input/finn_input.h"
#include "input/finn_event.h"
#include "utility/finn_error.h"

int finn_input_init() {
	g_input_state = malloc(sizeof(finn_input));
	if(g_input_state == NULL) {
		finn_error_add("Unable to allocate memory for g_input_state!");
		return 0;
	}
	finn_input_new_frame();
	g_input_state->mx = 0;
	g_input_state->my = 0;
	g_input_state->mousepress = FINN_PRESS_NONE;
	g_input_state->mousebutton = FINN_MOUSE_NONE;
	g_input_state->keymod = FINN_MOD_NONE;
	g_input_state->keyname = "";
	g_input_state->keypress = FINN_PRESS_NONE;
	return 1;
}

void finn_input_free() {
	free(g_input_state);
}

void finn_input_new_frame() {
	finn_events_clear();
	g_input_state->type = FINN_EVENT_NONE;
	g_input_state->keyname = "none";
	g_input_state->keypress = FINN_PRESS_NONE;
	g_input_state->keymod = FINN_MOD_NONE;
}

void finn_set_quit_event() {
	g_input_state->type |= FINN_QUIT;
	finn_event* e = malloc(sizeof(finn_event));
	e->type = FINN_QUIT;
	finn_event_add(e);
}

void finn_set_mouse_position(float mx, float my) {
	g_input_state->type |= FINN_MOUSE_MOVE;
	g_input_state->mx = mx;
	g_input_state->my = my;
	finn_add_mouse_event(FINN_MOUSE_MOVE, mx, my, FINN_MOUSE_NONE, FINN_PRESS_NONE);
}

void finn_get_mouse_position(float* mx, float* my) {
	*mx = g_input_state->mx;
	*my = g_input_state->my;
}

void finn_set_mouse_button(FINN_MOUSE_BUTTON button, FINN_BUTTON_PRESS press) {
	g_input_state->type |= FINN_MOUSE_PRESS;
	g_input_state->mousebutton = button;
	g_input_state->mousepress = press;
	finn_add_mouse_event(FINN_MOUSE_PRESS, g_input_state->mx, g_input_state->my, button, press);
}

void finn_get_mouse_button(FINN_MOUSE_BUTTON* button, FINN_BUTTON_PRESS* press) {
	*button = g_input_state->mousebutton;
	*press = g_input_state->mousepress;
}

void finn_set_key_press(const char* keyname, FINN_KEY_MOD mod, FINN_BUTTON_PRESS press) {
	g_input_state->type |= FINN_KEY_PRESS;
	g_input_state->keyname = keyname;
	g_input_state->keymod = mod;
	g_input_state->keypress = press;
	finn_add_keyboard_event(FINN_KEY_PRESS, keyname, mod, press);
}

void finn_get_key_press(const char* keyname, FINN_KEY_MOD* mod, FINN_BUTTON_PRESS* press) {
	keyname = g_input_state->keyname;
	*mod = g_input_state->keymod;
	*press = g_input_state->keypress;
}
