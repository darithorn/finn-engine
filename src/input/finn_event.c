#include <stdlib.h>
#include "input/finn_event.h"

// TODO(darithorn): use a memory region to avoid using malloc for every event
typedef struct finn_event_entry_s {
	finn_event* event;
	struct finn_event_entry_s* prev;
	struct finn_event_entry_s* next;
} finn_event_entry;

static struct {
	int count;
	finn_event_entry* head; // first
	finn_event_entry* tail; // last
} finn_event_queue;

void finn_events_clear() {
	if(finn_event_queue.count > 0) {
		finn_event_entry** curr = &finn_event_queue.head;
		for(;*curr;)
		{
			finn_event_entry* entry = *curr;
			curr = &entry->next;
			free(entry->event);
			free(entry);
		}
	}
	finn_event_queue.count = 0;
}

void finn_add_mouse_event(FINN_EVENT_TYPE type, float mx, float my, FINN_MOUSE_BUTTON button, FINN_BUTTON_PRESS state) {
	finn_mouse_event* me = malloc(sizeof(finn_mouse_event));
	if(me == NULL) {
		return;
	}
	me->mx = mx;
	me->my = my;
	me->button = button;
	me->state = state;
	finn_event* e = malloc(sizeof(finn_event));
	if(e == NULL) {
		// free(me);
		return;
	}
	e->type = type;
	e->mouse = *me;
	finn_event_add(e);
	free(me);
}

void finn_add_keyboard_event(FINN_EVENT_TYPE type, const char* keyname, FINN_KEY_MOD mod, FINN_BUTTON_PRESS state) {
	finn_keyboard_event* ke = malloc(sizeof(finn_keyboard_event));
	if(ke == NULL) {
		return;
	}
	ke->key = keyname;
	ke->mod = mod;
	ke->state = state;
	finn_event* e = malloc(sizeof(finn_event));
	if(e == NULL) {
		// free(ke);
		return;
	}
	e->type = type;
	e->keyboard = *ke;
	finn_event_add(e);
	free(ke);
}

void finn_event_add(finn_event* event) {
	finn_event_entry* entry = malloc(sizeof(finn_event_entry));
	entry->event = event;
	entry->prev = finn_event_queue.tail;
	entry->next = NULL;
	if(finn_event_queue.count == 0) {
		finn_event_queue.head = entry;
	} else {
		finn_event_queue.tail->next = entry;
	}
	finn_event_queue.tail = entry;
	finn_event_queue.count += 1;
}

int finn_event_poll(finn_event* event) {
	if(finn_event_queue.count == 0) {
		return 0;
	}
	*event = *(finn_event_queue.tail->event);
	finn_event_queue.count -= 1;
	finn_event_entry* t = finn_event_queue.tail;
	if(finn_event_queue.tail->prev != NULL) {
		finn_event_queue.tail = t->prev;
	}
	finn_event_queue.tail->next = NULL;
	free(t->event);
	free(t);
	return 1;
}
