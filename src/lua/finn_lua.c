#include "lua/finn_lua.h"
#include "lua/finn_lua_utility.h"
#include "lua/finn_sprite_lua.h"
#include "lua/finn_draw_utility_lua.h"
#include "lua/finn_font_lua.h"
#include "lua/finn_root_lua.h"
#include "lua/finn_image_lua.h"
#include "input/finn_input.h"
#include "utility/finn_error.h"
#include "graphics/finn_draw_utility.h"
#include "graphics/finn_root.h"

lua_State* g_lua_state;

// Called-from-C Functions
// init()
void finn_lua_func_init(lua_State* l) {
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "init");
	lua_pcall(l, 0, 0, 0);
	finn_lua_clear_stack(l);
}
// update(dt)
void finn_lua_func_update(lua_State* l, float dt) {
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "update");
	lua_pushnumber(l, dt);
	lua_pcall(l, 1, 0, 0);
	finn_lua_clear_stack(l);
}
// render()
void finn_lua_func_render(lua_State* l) {
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "render");
	lua_pcall(l, 0, 0, 0);
	finn_lua_clear_stack(l);
}
// onMouseMove(x, y)
void finn_lua_on_mouse_move(lua_State* l) {
	if((g_input_state->type & FINN_MOUSE_MOVE) != FINN_MOUSE_MOVE) {
		return;
	}
	
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "onMouseMove");
	lua_pushinteger(l, g_input_state->mx);
	lua_pushinteger(l, g_input_state->my);
	lua_pcall(l, 2, 0, 0);
	finn_lua_clear_stack(l);
}
// onMousePress(button)
void finn_lua_on_mouse_pressed(lua_State* l) {
	if((g_input_state->type & FINN_MOUSE_PRESS) != FINN_MOUSE_PRESS) {
		return;
	}
	if(g_input_state->mousepress != FINN_PRESS_DOWN) {
		return;
	}
	
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "onMousePressed");
	
	switch(g_input_state->mousebutton) {
		case FINN_MOUSE_LEFT:
			lua_pushstring(l, "left");
			break;
		case FINN_MOUSE_RIGHT:
			lua_pushstring(l, "right");
			break;
		case FINN_MOUSE_MIDDLE:
			lua_pushstring(l, "middle");
			break;
		case FINN_MOUSE_NONE:
			lua_pushstring(l, "none");
			break;
	}
	lua_pcall(l, 1, 0, 0);
	finn_lua_clear_stack(l);
}

void finn_lua_on_mouse_released(lua_State* l) {
	if((g_input_state->type & FINN_MOUSE_PRESS) != FINN_MOUSE_PRESS) {
		return;
	}
	if(g_input_state->mousepress != FINN_PRESS_UP) {
		return;
	}
	
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "onMouseReleased");
	
	switch(g_input_state->mousebutton) {
		case FINN_MOUSE_LEFT:
			lua_pushstring(l, "left");
			break;
		case FINN_MOUSE_RIGHT:
			lua_pushstring(l, "right");
			break;
		case FINN_MOUSE_MIDDLE:
			lua_pushstring(l, "middle");
			break;
		case FINN_MOUSE_NONE:
			lua_pushstring(l, "none");
			break;
	}
	lua_pcall(l, 1, 0, 0);
	finn_lua_clear_stack(l);
}

// onKeyPress(key, mod)
void finn_lua_on_key_pressed(lua_State* l) { 
	if((g_input_state->type & FINN_KEY_PRESS) != FINN_KEY_PRESS) {
		return;
	}
	if(g_input_state->keypress != FINN_PRESS_DOWN) {
		return;
	}
	
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "onKeyPressed");
	
	lua_pushstring(l, g_input_state->keyname);
	switch(g_input_state->keymod) {
		case FINN_MOD_NONE:
			lua_getfield(l, -3, "none");
			break;
		case FINN_MOD_SHIFT:
			lua_getfield(l, -3, "shift");
			break;
		case FINN_MOD_CONTROL:
			lua_getfield(l, -3, "ctrl");
			break;
		case FINN_MOD_ALT:
			lua_getfield(l, -3, "alt");
			break;
	}
	lua_pcall(l, 2, 0, 0);
	finn_lua_clear_stack(l);
}

// onKeyReleased(key, mod)
void finn_lua_on_key_released(lua_State* l) {
	if((g_input_state->type & FINN_KEY_PRESS) != FINN_KEY_PRESS) {
		return;
	}
	if(g_input_state->keypress != FINN_PRESS_UP) {
		return;
	}
	
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "onKeyReleased");
	
	lua_pushstring(l, g_input_state->keyname);
	switch(g_input_state->keymod) {
		case FINN_MOD_NONE:
			lua_getfield(l, -3, "none");
			break;
		case FINN_MOD_SHIFT:
			lua_getfield(l, -3, "shift");
			break;
		case FINN_MOD_CONTROL:
			lua_getfield(l, -3, "ctrl");
			break;
		case FINN_MOD_ALT:
			lua_getfield(l, -3, "alt");
			break;
	}
	lua_pcall(l, 2, 0, 0);
	finn_lua_clear_stack(l);
}

void finn_lua_quit(lua_State* l) {
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "quit");
	lua_pcall(l, 0, 0, 0);
	finn_lua_clear_stack(l);
}

void finn_lua_update_mouse(lua_State* l) {
	// Update Mouse Button
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "mouse");
	lua_pushnumber(l, g_input_state->mx);
	lua_setfield(l, -2, "x");
	finn_lua_clear_stack(l);
	
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "mouse");
	lua_pushnumber(l, g_input_state->my);
	lua_setfield(l, -2, "y");
	finn_lua_clear_stack(l);
	
	// Update Mouse State
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "mouse");
	switch(g_input_state->mousepress) {
		case FINN_PRESS_UP:
			lua_pushstring(l, "released");
			break;
		case FINN_PRESS_DOWN:
			lua_pushstring(l, "pressed");
			break;
		default:
			lua_pushstring(l, "none");
			break;
	}
	lua_setfield(g_lua_state, -2, "state");
	finn_lua_clear_stack(l);
	
	// update button
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, "mouse");
	switch(g_input_state->mousebutton) {
		case FINN_MOUSE_LEFT:
			lua_pushstring(l, "left");
			break;
		case FINN_MOUSE_RIGHT:
			lua_pushstring(l, "right");
			break;
		case FINN_MOUSE_MIDDLE:
			lua_pushstring(l, "middle");
			break;
		default:
			lua_pushstring(l, "none");
			break;
	}
	lua_setfield(g_lua_state, -2, "button");
	finn_lua_clear_stack(l);
}

void finn_lua_push_function(lua_State* l, const char* subtable, const char* name, lua_CFunction func) {
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, subtable);
	lua_pushcfunction(l, func);
	lua_setfield(l, -2, name);
	finn_lua_clear_stack(l);
}

void finn_lua_push_string_field(lua_State* l, const char* name) {
	lua_getglobal(l, "finn");
	lua_pushstring(l, name);
	lua_setfield(l, -2, name);
	finn_lua_clear_stack(l);
}

void finn_lua_push_string_field_sub(lua_State* l, const char* subtable, const char* name) {
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, subtable);
	lua_pushstring(l, name);
	lua_setfield(l, -2, name);
	finn_lua_clear_stack(l);
}

void finn_lua_push_number_field(lua_State* l, const char* subtable, const char* name, double value) {
	lua_getglobal(l, "finn");
	lua_getfield(l, -1, subtable);
	lua_pushnumber(l, value);
	lua_setfield(l, -2, name);
	finn_lua_clear_stack(l);
}

void finn_lua_push_table(lua_State* l, const char* name) {
	lua_getglobal(l, "finn");
	lua_newtable(l);
	lua_setfield(l, -2, name);
	finn_lua_clear_stack(l);
}

int finn_lua_load(const char* file) {
	g_lua_state = luaL_newstate();
	luaL_openlibs(g_lua_state);
	
	lua_createtable(g_lua_state, 0, 0);
	lua_setglobal(g_lua_state, "finn");
	
	finn_lua_push_table(g_lua_state, "root");
	finn_lua_push_table(g_lua_state, "graphics");
	finn_lua_push_table(g_lua_state, "sprite");
	finn_lua_push_table(g_lua_state, "font");
	finn_lua_push_table(g_lua_state, "image");
	finn_lua_push_table(g_lua_state, "text");
	finn_lua_push_table(g_lua_state, "screen");
	finn_lua_push_table(g_lua_state, "mouse");
	
	// Push all finn variables
	finn_lua_push_string_field(g_lua_state, "none");
	finn_lua_push_string_field(g_lua_state, "released");
	finn_lua_push_string_field(g_lua_state, "pressed");
	finn_lua_push_string_field(g_lua_state, "shift");
	finn_lua_push_string_field(g_lua_state, "ctrl");
	finn_lua_push_string_field(g_lua_state, "alt");
	finn_lua_push_number_field(g_lua_state, "font", "normal", 0);
	finn_lua_push_number_field(g_lua_state, "font", "italic", 1);
	finn_lua_push_number_field(g_lua_state, "font", "oblique", 2);
	finn_lua_push_number_field(g_lua_state, "font", "bold", 1);
	
	finn_lua_push_string_field_sub(g_lua_state, "mouse", "left");
	finn_lua_push_string_field_sub(g_lua_state, "mouse", "right");
	finn_lua_push_string_field_sub(g_lua_state, "mouse", "middle");
	finn_lua_push_string_field_sub(g_lua_state, "mouse", "state");
	finn_lua_push_string_field_sub(g_lua_state, "mouse", "button");
	finn_lua_push_number_field(g_lua_state, "mouse", "x", 0);
	finn_lua_push_number_field(g_lua_state, "mouse", "y", 0);
	finn_lua_push_number_field(g_lua_state, "screen", "width", g_root->width);
	finn_lua_push_number_field(g_lua_state, "screen", "height", g_root->height);
	
	// Push all finn functions
	finn_lua_push_function(g_lua_state, "root", "limitFPS", &finn_lua_root_limit_fps);
	
	// finn_sprite.h functions
	finn_lua_push_function(g_lua_state, "sprite", "createFromPNG", &finn_lua_sprite_create_from_png);
	finn_lua_push_function(g_lua_state, "sprite", "createFromSVG", &finn_lua_sprite_create_from_svg);
	finn_lua_push_function(g_lua_state, "sprite", "createFromImage", &finn_lua_sprite_create_from_image);
	finn_lua_push_function(g_lua_state, "sprite", "draw", &finn_lua_sprite_draw);
	finn_lua_push_function(g_lua_state, "sprite", "getFile", &finn_lua_sprite_get_file);
	finn_lua_push_function(g_lua_state, "sprite", "getWidth", &finn_lua_sprite_get_width);
	finn_lua_push_function(g_lua_state, "sprite", "getHeight", &finn_lua_sprite_get_height);
	finn_lua_push_function(g_lua_state, "sprite", "setRegion", &finn_lua_sprite_set_region);
	finn_lua_push_function(g_lua_state, "sprite", "resize", &finn_lua_sprite_resize);
	finn_lua_push_function(g_lua_state, "sprite", "resizeAspRat", &finn_lua_sprite_resize_aspect_ratio);
	
	// finn_draw_utility.h functions
	finn_lua_push_function(g_lua_state, "graphics", "clearColor", &finn_lua_fill_background_color);
	
	// text related functions
	finn_lua_push_function(g_lua_state, "text", "setColor", &finn_lua_set_text_color);
	finn_lua_push_function(g_lua_state, "text", "draw", &finn_lua_draw_text);
	
	// finn_font.h functions
	finn_lua_push_function(g_lua_state, "font", "create", &finn_lua_font_create);
	finn_lua_push_function(g_lua_state, "font", "getSize", &finn_lua_font_get_size);
	finn_lua_push_function(g_lua_state, "font", "set", &finn_lua_set_font);
	
	// finn_image.h functions
	finn_lua_push_function(g_lua_state, "image", "createFromPNG", &finn_lua_image_create_from_png);
	finn_lua_push_function(g_lua_state, "image", "createFromSVG", &finn_lua_image_create_from_svg);
	finn_lua_push_function(g_lua_state, "image", "getWidth", &finn_lua_image_get_width);
	finn_lua_push_function(g_lua_state, "image", "getHeight", &finn_lua_image_get_height);

	int status = luaL_loadfile(g_lua_state, file);
	if(status != LUA_OK) {
		finn_error_add(lua_tostring(g_lua_state, -1));
		return 0;
	}
	//finn_initFuncLua(g_lua_state);
	// printf("Stack: %i\n", lua_gettop(g_lua_state));
	
	int result = lua_pcall(g_lua_state, 0, LUA_MULTRET, 0);
	if(result != LUA_OK) {
		finn_error_add(lua_tostring(g_lua_state, -1));
		return 0;
	}
	return 1;
}

void finn_lua_cleanup() {
	finn_lua_send_quit();
	if(g_lua_state != NULL) {
		lua_close(g_lua_state);	
	}
}

void finn_lua_send_quit() {
	if(g_lua_state != NULL) {
		finn_lua_quit(g_lua_state);
	}
}

void finn_lua_update(float dt) {
	if(g_lua_state != NULL) {
		finn_lua_update_mouse(g_lua_state);

		finn_lua_on_key_pressed(g_lua_state);
		finn_lua_on_key_released(g_lua_state);
		finn_lua_on_mouse_pressed(g_lua_state);
		finn_lua_on_mouse_released(g_lua_state);
		finn_lua_on_mouse_move(g_lua_state);
		finn_lua_func_update(g_lua_state, dt);
		finn_lua_func_render(g_lua_state);
	}
}
