#include "lua/finn_sprite_lua.h"
#include "lua/finn_lua_utility.h"
#include "utility/finn_error.h"
#include "graphics/finn_sprite.h"

// createSprite(file)
int finn_lua_sprite_create_from_png(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.createSprite requires 1 parameter!");
		return 0;
	}
	const char* file;
	if(!lua_isstring(l, 1)) {
		finn_error_add("First argument of finn.createSprite must be a string!");
		return 0;
	}
	file = lua_tostring(l, 1);
	
	finn_sprite* sprite = finn_sprite_create_from_png(file);
	if(sprite == NULL) {
		return 0;
	}
	finn_lua_clear_stack(l);
	lua_pushlightuserdata(l, sprite);
	return 1;
}

int finn_lua_sprite_create_from_svg(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.createSprite requires 1 parameter!");
		return 0;
	}
	const char* file;
	if(!lua_isstring(l, 1)) {
		finn_error_add("First argument of finn.createSprite must be a string!");
		return 0;
	}
	file = lua_tostring(l, 1);
	
	finn_sprite* sprite = finn_sprite_create_from_svg(file);
	if(sprite == NULL) {
		return 0;
	}
	finn_lua_clear_stack(l);
	lua_pushlightuserdata(l, sprite);
	return 1;
}

// finn.createSpriteFromImage(img)
int finn_lua_sprite_create_from_image(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.createSpriteFromImage requires 1 argument!");
		return 0;
	}
	finn_image* img;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.createSpriteFromImage must be an image!");
		return 0;
	}
	img = (finn_image*)lua_touserdata(l, 1);
	
	finn_sprite* sprite = finn_sprite_create_from_image(img);
	if(sprite == NULL) {
		return 0;
	}
	finn_lua_clear_stack(l);
	lua_pushlightuserdata(l, sprite);
	return 1;
}

// drawSprite(sprite, x, y)
int finn_lua_sprite_draw(lua_State* l) {
	if(lua_gettop(l) != 3) {
		finn_error_add("finn.drawSprite requires 3 arguments!");
		return 0;
	}
	finn_sprite* sprite;
	int x, y;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.drawSprite must be a sprite!");
		return 0;
	}
	sprite = (finn_sprite*)lua_touserdata(l, 1);
	
	if(!lua_isnumber(l, 2)) {
		finn_error_add("Second argument of finn.drawSprite must be a number!");
		return 0;
	}
	x = lua_tonumber(l, 2);
	
	if(!lua_isnumber(l, 3)) {
		finn_error_add("Third argument of finn.drawSprite must be a number!");
		return 0;
	}
	y = lua_tonumber(l, 3);
	
	finn_sprite_draw(sprite, x, y);
	finn_lua_clear_stack(l);
	return 0;
}

// getSpriteFile(sprite)
int finn_lua_sprite_get_file(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.getSpriteFile requires 1 argument!");
		return 0;
	}
	finn_sprite* sprite;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.getSpriteFile must be a sprite!");
		return 0;
	}
	sprite = (finn_sprite*)lua_touserdata(l, 1);
	finn_lua_clear_stack(l);
	lua_pushstring(l, sprite->img->file);
	return 1;
}

// getSpriteWidth(sprite)
int finn_lua_sprite_get_width(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.getSpriteWidth requires 1 argument!");
		return 0;
	}
	finn_sprite* sprite;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.getSpriteWidth must be a sprite!");
		return 0;
	}
	sprite = (finn_sprite*)lua_touserdata(l, 1);
	finn_lua_clear_stack(l);
	lua_pushinteger(l, sprite->img->width);
	return 1;
}

// getSpriteHeight(sprite)
int finn_lua_sprite_get_height(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.getSpriteHeight requires 1 argument!");
		return 0;
	}
	finn_sprite* sprite;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.getSpriteHeight must be a sprite!");
		return 0;
	}
	sprite = (finn_sprite*)lua_touserdata(l, 1);
	finn_lua_clear_stack(l);
	lua_pushinteger(l, sprite->img->height);
	return 1;
}

// setSpriteRegion(sprite, x, y, w, h)
int finn_lua_sprite_set_region(lua_State* l) {
	if(lua_gettop(l) != 5) {
		finn_error_add("finn.setSpriteRegion requires 5 arguments!");
		return 0;
	}
	finn_sprite* sprite;
	int x, y, w, h;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.setSpriteRegion must be a sprite!");
		return 0;
	}
	sprite = (finn_sprite*)lua_touserdata(l, 1);
	
	if(!lua_isnumber(l, 2)) {
		finn_error_add("Second argument of finn.setSpriteRegion must be a number!");
		return 0;
	}
	x = lua_tonumber(l, 2);
	
	if(!lua_isnumber(l, 3)) {
		finn_error_add("Third argument of finn.setSpriteRegion must be a number!");
		return 0;
	}
	y = lua_tonumber(l, 3);
	
	if(!lua_isnumber(l, 4)) {
		finn_error_add("Fourth argument of finn.setSpriteRegion must be a number!");
		return 0;
	}
	w = lua_tonumber(l, 4);
	
	if(!lua_isnumber(l, 5))	{
		finn_error_add("Fifth argument of finn.setSpriteRegion must be a number!");
		return 0;
	}
	h = lua_tonumber(l, 5);
	
	finn_sprite_set_region(sprite, x, y, w, h);
	finn_lua_clear_stack(l);
	return 0;
}

// finn.resizeSprite(sprite, w, h)
int finn_lua_sprite_resize(lua_State* l) {
	if(lua_gettop(l) != 3) {
		finn_error_add("finn.resizeSprite requires 3 arguments!");
		return 0;
	}
	finn_sprite* sprite;
	float w, h;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.resizeSprite must be a sprite!");
		return 0;
	}
	sprite = (finn_sprite*)lua_touserdata(l, 1);
	
	if(!lua_isnumber(l, 2)) {
		finn_error_add("Second argument of finn.resizeSprite must be a number!");
		return 0;
	}
	w = lua_tonumber(l, 2);
	
	if(!lua_isnumber(l, 3)) {
		finn_error_add("Third argument of finn.resizeSprite must be a number!");
		return 0;
	}
	h = lua_tonumber(l, 3);
	
	finn_sprite_resize(sprite, w, h);
	finn_lua_clear_stack(l);
	return 0;
}

// finn.resizeSprite(sprite, w, h)
int finn_lua_sprite_resize_aspect_ratio(lua_State* l) {
	if(lua_gettop(l) != 2) {
		finn_error_add("finn.resizeSprite requires 3 arguments!");
		return 0;
	}
	finn_sprite* sprite;
	float scale;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.resizeSprite must be a sprite!");
		return 0;
	}
	sprite = (finn_sprite*)lua_touserdata(l, 1);
	
	if(!lua_isnumber(l, 2)) {
		finn_error_add("Second argument of finn.resizeSprite must be a number!");
		return 0;
	}
	scale = lua_tonumber(l, 2);
	
	finn_sprite_resize_aspect_ratio(sprite, scale);
	finn_lua_clear_stack(l);
	return 0;
}
