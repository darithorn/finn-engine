#include "lua/finn_image_lua.h"
#include "lua/finn_lua_utility.h"
#include "graphics/finn_image.h"
#include "utility/finn_error.h"

// finn.createImage(file)
int finn_lua_image_create_from_png(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.image.createFromPNG requires 1 argument!");
		return 0;
	}
	const char* file;
	if(!lua_isstring(l, 1))	{
		finn_error_add("First argument of finn.image.createFromPNG must be a string!");
		return 0;
	}
	file = lua_tostring(l, 1);
	
	finn_image* image = finn_image_create_from_png(file);
	if(image == NULL) {
		return 0;
	}
	finn_lua_clear_stack(l);
	lua_pushlightuserdata(l, image);
	return 1;
}

int finn_lua_image_create_from_svg(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.image.createFromSVG requires 1 argument!");
		return 0;
	}
	const char* file;
	if(!lua_isstring(l, 1)) {
		finn_error_add("First argument of finn.image.createFromSVG must be a string!");
		return 0;
	}
	file = lua_tostring(l, 1);
	
	finn_image* image = finn_image_create_from_svg(file);
	if(image == NULL) {
		return 0;
	}
	finn_lua_clear_stack(l);
	lua_pushlightuserdata(l, image);
	return 1;
}

// finn.getImageWidth(img)
int finn_lua_image_get_width(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.getImageWidth requires 1 argument!");
		return 0;
	}
	finn_image* img;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.getImageWidth must be a image!");
		return 0;
	}
	img = (finn_image*)lua_touserdata(l, 1);
	
	finn_lua_clear_stack(l);
	lua_pushnumber(l, img->width);
	return 1;
}

// finn.getImageWidth(img)
int finn_lua_image_get_height(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.getImageHeight requires 1 argument!");
		return 0;
	}
	finn_image* img;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.getImageHeight must be a image!");
		return 0;
	}
	img = (finn_image*)lua_touserdata(l, 1);
	
	finn_lua_clear_stack(l);
	lua_pushnumber(l, img->height);
	return 1;
}
