#include "lua/finn_draw_utility_lua.h"
#include "lua/finn_lua_utility.h"
#include "utility/finn_error.h"
#include "graphics/finn_root.h"
#include "graphics/finn_draw_utility.h"

int finn_lua_fill_background_color(lua_State* l) {
	int args = lua_gettop(l);
	if(args != 3 && args != 4) {
		finn_error_add("finn.fillBackgroundColor requires either 3 or 4 arguments!");
		return 0;
	}
	float r, g, b;
	int n;
	
	if(!lua_isnumber(l, 1)) {
		finn_error_add("First argument of finn.fillBackgroundColor must be a number!");
		return 0;
	}
	r = lua_tonumber(l, 1);
	
	if(!lua_isnumber(l, 2)) {
		finn_error_add("Second argument of finn.fillBackgroundColor must be a number!");
		return 0;
	}
	g = lua_tonumber(l, 2);
	
	if(!lua_isnumber(l, 3))	{
		finn_error_add("Third argument of finn.fillBackgroundColor must be a number!");
		return 0;
	}
	b = lua_tonumber(l, 3);
	
	if(args == 4) {
		if(!lua_isboolean(l, 4)) {
			finn_error_add("Fourth argument of finn.fillBackgroundColor must be a boolean!");
			return 0;
		}
		n  = lua_toboolean(l, 4);
		if(n) {
			// finn_texture_fill_background_normalized_rgb(g_root->texture, r, g, b);
		} else {
			// finn_texture_fill_background_rgb(g_root->texture, r, g, b);
		}
	} else {
		// finn_texture_fill_background_rgb(g_root->texture, r, g, b);
	}
	finn_lua_clear_stack(l);
	return 0;
}

int finn_lua_set_font(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.setFont requires 1 argument!");
		return 0;
	}
	finn_font* font;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.setFont must be a font!");
		return 0;
	}
	font = (finn_font*)lua_touserdata(l, 1);
	
	finn_root_set_font(font);
	finn_lua_clear_stack(l);
	return 0;
}

int finn_lua_set_text_color(lua_State* l) {
	if(lua_gettop(l) != 3) {
		finn_error_add("finn.setTextColor requires 3 arguments!");
		return 0;
	}
	float r, g, b;
	if(!lua_isnumber(l, 1)) {
		finn_error_add("First argument of finn.setTextColor must be a number!");
		return 0;
	}
	r = lua_tonumber(l, 1);
	
	if(!lua_isnumber(l, 2)) {
		finn_error_add("Second argument of finn.setTextColor must be a number!");
		return 0;
	}
	g = lua_tonumber(l, 2);
	
	if(!lua_isnumber(l, 3)) {
		finn_error_add("Third argument of finn.setTextColor must be a number!");
		return 0;
	}
	b = lua_tonumber(l, 3);
	
	finn_root_set_text_rgb(r, g, b);
	finn_lua_clear_stack(l);
	return 0;
}

// drawText(text, x, y)
int finn_lua_draw_text(lua_State* l) { 
	if(lua_gettop(l) != 3) {
		finn_error_add("finn.drawText requires 3 arguments!");
		return 0;
	}
	const char* text;
	int x, y;
	if(!lua_isstring(l, 1))	{
		finn_error_add("First argument of finn.drawText must be a string!");
		return 0;
	}
	text = lua_tostring(l, 1);
	
	if(!lua_isnumber(l, 2))	{
		finn_error_add("Second argument of finn.drawText must be a number!");
		return 0;
	}
	x = lua_tointeger(l, 2);
	
	if(!lua_isnumber(l, 3))	{
		finn_error_add("Third argument of finn.drawText must be a number!");
		return 0;
	}
	y = lua_tointeger(l, 3);
	
	finn_root_draw_text(text, x, y);
	finn_lua_clear_stack(l);
	return 0; 
}
