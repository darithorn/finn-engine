#include "lua/finn_lua_utility.h"

void finn_lua_clear_stack(lua_State* l) {
	lua_pop(l, lua_gettop(l));
}

void finn_lua_clear_stack_offset(lua_State* l, int offset) {
	lua_pop(l, lua_gettop(l) - offset);
}
