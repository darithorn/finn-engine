#include "lua/finn_root_lua.h"
#include "lua/finn_lua_utility.h"
#include "graphics/finn_root.h"
#include "utility/finn_error.h"

int finn_lua_root_limit_fps(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.limitFPS requires 1 argument!");
		return 0;
	}
	int fps;
	if(!lua_isnumber(l, 1)) {
		finn_error_add("First argument of finn.limitFPS must be a number!");
		return 0;
	}
	fps = lua_tonumber(l, 1);
	finn_root_limit_fps(fps);
	finn_lua_clear_stack(l);
	return 0;
}
