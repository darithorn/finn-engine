#include "lua/finn_font_lua.h"
#include "lua/finn_lua_utility.h"
#include "utility/finn_error.h"
#include "graphics/finn_font.h"

// finn.createFont(family, size, slant, weight)
// finn.createFont(family, size) slant -> normal - weight -> normal
int finn_lua_font_create(lua_State* l) {
	int args = lua_gettop(l);
	if(args != 2 && args != 4) {
		finn_error_add("finn.createFont requires either 2 or 4 arguments!");
		return 0;
	}
	const char* family;
	int size;
	// 0 = normal
	// 1 = italic
	// 2 = oblique
	int slant = 0;
	// 0 = normal
	// 1 = bold
    int weight = 0;
	
	if(!lua_isstring(l, 1))	{
		finn_error_add("First argument of finn.createFont must be a string!");
		return 0;
	}
	family = lua_tostring(l, 1);
	
	if(!lua_isnumber(l, 2))	{
		finn_error_add("Second argument of finn.createFont must be a number!");
		return 0;
	}
	size = lua_tonumber(l, 2);
	
	if(args == 4) {
		if(!lua_isnumber(l, 3))	{
			finn_error_add("Third argument of finn.createFont must be a finn.slant!");
			return 0;
		}
		slant = lua_tonumber(l, 3);
		if(!lua_isnumber(l, 4)) {
			finn_error_add("Fourth argument of finn.createFont must be a finn.weight!");
			return 0;
		}
		weight = lua_tonumber(l, 4);
	}
	FINN_FONT_SLANT s = -1;
	if(slant == 0) {
		s = FINN_SLANT_NORMAL;
	} else if(slant == 1) {
		s = FINN_SLANT_ITALIC;
	} else if(slant == 2) {
		s = FINN_SLANT_OBLIQUE;
	}
	
	if(s == -1) {
		finn_error_add("Font slant must be either: finn.normal, finn.italic or finn.oblique!");
		return 0;
	}
	FINN_FONT_WEIGHT w = -1;
	if(weight == 0) {
		w = FINN_WEIGHT_NORMAL;
	} else if(weight == 1) {
		w = FINN_WEIGHT_BOLD;
	}
	
	if(w == -1) {
		finn_error_add("Font weight must be either: finn.normal or finn.bold!");
		return 0;
	}
	finn_font* font = finn_font_create(family, size, s, w);
	
	finn_lua_clear_stack(l);
	lua_pushlightuserdata(l, font);
	return 1;
}

// finn.setFontSize(font, size)
int finn_lua_font_set_size(lua_State* l) {
	return 0;
}

// finn.getFontSize(font)
int finn_lua_font_get_size(lua_State* l) {
	if(lua_gettop(l) != 1) {
		finn_error_add("finn.getFontSize requires 1 argument!");
		return 0;
	}
	finn_font* font;
	if(!lua_isuserdata(l, 1)) {
		finn_error_add("First argument of finn.getFontSize must be a font!");
		return 0;
	}
	font = (finn_font*)lua_touserdata(l, 1);
	
	finn_lua_clear_stack(l);
	lua_pushnumber(l, font->size);
	return 1;
}
