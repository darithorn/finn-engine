#include <stdio.h>
#ifdef _WIN32
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif
#include "finn.h"

void print_help () {
	printf ("Usage: ./finn-engine [file]\n");
}

int main(int argc, char **argv) {
	if(argc < 2) {
		print_help();
		return 0;
	}
	
	int32_t width = 800;
	int32_t height = 480;
	int running = 1;
    
	finn_program* program = finn_program_create_init("Hello World!", width, height, 
													 FINN_INIT_EVERYTHING, SDL_INIT_VIDEO);
	
	finn_lua_load(argv[1]);
	finn_event event;
	while(running) {
		while (finn_has_error ()) {
			printf ("%s\n", finn_error_get ());
		}
		
		while(finn_event_poll(&event)) {
			switch(event.type) {
			case FINN_MOUSE_MOVE:
				break;
			case FINN_MOUSE_PRESS:
				break;
			case FINN_KEY_PRESS:
				break;
			case FINN_QUIT:
				running = 0;
				break;
			default:
				break;
			}
		}
		finn_program_clear_background();
		
		
		
		finn_program_swap_buffer(program);
	}
	
	finn_program_destroy_exit(program);
	
	return 0;
}
