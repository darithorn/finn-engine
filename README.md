[![Build Status](https://travis-ci.org/darithorn/finn-engine.svg?branch=master)](https://travis-ci.org/darithorn/finn-engine)

This is a 2D game engine written in C. It builds into a library you link to create an initial program in C. You can use Lua for the rest.

# Directories

* **build/** - includes premake4 file for building the project
* **lib/** - this contains a helper library to use in Lua.
* **include/** - the headers for the project
* **src/** - the source code for the project

*This is a hobby project. There are no guarantees! I will work on this on my freetime.*
