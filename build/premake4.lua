solution "finn-engine"
    configurations { "Debug", "Release" }
	configuration { "linux" }
	    buildoptions { "`pkg-config --cflags glib-2.0`" }
        includedirs {  
	  	     "../include/",	
	   	     "/usr/include/gdk-pixbuf-2.0",
	   	     "/usr/include/cairo",
	   	  }

project "finn-engine"
    kind "ConsoleApp"
	language "C"
	files { "../src/**.h", "../src/**.c" }
	links { "m", "GL", "glib-2.0", "gobject-2.0", "cairo", "rsvg-2", "lua5.2", "SDL2" }
	linkoptions { "-Wl,-rpath ./", "-Wl,--export-dynamic" }
    configuration { "Debug" }
	    defines { "DEBUG" }  
	    flags { "Symbols" }
	    targetdir "../bin/debug"
	configuration { "Release" }
	    defines { "RELEASE" }
        targetdir "../bin/release"
