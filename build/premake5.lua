solution "finn-engine"
    configurations { "DEBUG", "RELEASE" }
    configuration { "linux" }
	    buildoptions { "`pkg-config --cflags glib-2.0`" }
        includedirs {  
	  	     "../include/",	
	   	     "/usr/include/gdk-pixbuf-2.0",
			 "/usr/include/cairo",
	   	  }
	configuration { "windows" }
	-- TODO(darithorn): add an x86 and x64 configuration
		libdirs { "../external/", "../external/lua5.2/", "../external/SDL2/lib/x86/", "../external/nanovg/" }
		includedirs { "../include/", "../external/", "../external/SDL2/include/", "../external/lua5.2/", "../external/nanovg" }
project "finn-engine"
    kind "ConsoleApp"
	language "C"
	files { "../src/**.h", "../src/**.c" }
	configuration { "linux" }
		links { "m", "GL", "glib-2.0", "gobject-2.0", "cairo", "rsvg-2", "lua5.2", "SDL2" }
		defines { "__LIBCAIRO__", "__LIBRSVG__", "__IMM_MODE_SUPP__" }
	configuration { "windows" }
		links { "gdi32", "user32", "opengl32", "kernel32", "lua52", "SDL2main", "SDL2", "nanovg" }
		defines { "__LIBNANOVG__"}
    configuration { "DEBUG" }
	    defines { "DEBUG" }  
	    flags { "Symbols" }
	    targetdir "../bin/debug"
    configuration { "RELEASE" }
	    defines { "RELEASE" }
        targetdir "../bin/release"
