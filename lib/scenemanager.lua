require "lib/class"

class "scenemanager"

function scenemanager:scenemanager()
   self.scenes = {}
   self.currentScene = nil
end

function scenemanager:addScene(scene)
   self.scenes[scene.name] = scene
end

function scenemanager:loadScene(name)
   local temp = self.scenes[name]
   if temp ~= nil then
	  self.currentScene = temp
   else
	  error("Cannot find scene with name: "name .. "!")
   end
end

function scenemanager:updateCurrent(dt)
   if self.currentScene ~= nil then
	  self.currentScene.entityManager.updateEntities(dt)
   end
end

function scenemanager:renderCurrent(dt)
   if self.currentScene ~= nil then
	  self.currentScene.entityManager.renderEntities(dt)
   end
end
