require "lib/class"
require "lib/utility"

class "entitymanager"

function entitymanager:entitymanager()
	self.entities = {}
	self.previoushover = nil
	self.previousclicked = nil
	self.entitydragging = nil
	self.lastClickedX, self.lastClickedY = 0, 0
end

function entitymanager:sortByLayer()
	table.sort(self.entities, function(a, b) return a:layer() < b:layer() end)
end

function entitymanager:setToTop(ent)
	local topLayer = #self.entities
	if ent:layer() >= topLayer then 
		return 
	end
	local entLayer = ent:layer()
	-- decrease all entity layers by 1 to avoid ever increasing layers
	for i, _ in ipairs(self.entities) do
		local e = self.entities[i]
		if e == ent then
			ent:layer(topLayer) 
		elseif e:layer() >= entLayer then
			e:layer(e:layer() - 1)
		end
	end
	self:sortByLayer()
end

function entitymanager:addEntity(e)
	table.insert(self.entities, e)
	return #self.entities
end

function entitymanager:getTopEntity(cond)
	if cond == nil then cond = function(e) return false end end
	for i = #self.entities, 1, -1 do
		local e = self.entities[i]
		if not cond(e) then return e end
	end
	return nil
end

function entitymanager:getTopEntityInArray(a, ordered, cond)
	if type(ordered) == "function" then 
		cond = ordered
		ordered = false
	end
	if ordered == nil then ordered = false end
	if cond == nil then cond = function(e) return false end end
	if ordered then
		for i = #a, 1, -1 do
			local e = self.entities[i]
			if not cond(e) then return e end
		end
		return nil
	else
		if cond == nil then cond = function(e) return false end end
		local highest = nil
		for _, e in pairs(a) do
			if not cond(e) and (highest == nil or highest:layer() < e:layer()) then
				highest = e
			end
		end
		return highest
	end
end

function entitymanager:getTopEntityAtMouse(mx, my, cond)
	if cond == nil then cond = function(e) return false end end
	for i = #self.entities, 1, -1 do
		local e = self.entities[i]
		if not cond(e) and e:rect():contains(mx, my) then return e end
	end
	return nil
end

function entitymanager:findEntity(test)
	for _, e in pairs(self.entities) do
		if test(e) then return e end
	end
	return nil
end

function entitymanager:findEntities(test)
	local t = {}
	for _, e in pairs(self.entities) do
		if test(e) then table.insert(t, e) end
	end
	return t
end

function entitymanager:findEntityByName(name)
	return self:findEntity(function(e) return e.name == name end)
end

function entitymanager:findEntityByTag(tag)
	return self:findEntity(function(e) return e.tag == tag end)
end

function entitymanager:findEntitiesByName(name)
	return self:findEntities(function(e) return e.name == name end)
end

function entitymanager:findEntitiesByTag(tag)
	return self:findEntities(function(e) return e.tag == tag end)
end

function entitymanager:updateEntities(dt)
	for _, e in pairs(self.entities) do
		e:update(dt)
	end
end

function entitymanager:renderEntities()
	for i=1, #self.entities, 1 do
		if self.entities[i].active then self.entities[i]:render() end
	end
end

function entitymanager:onDrop()
	local inside = {}
	for _, e in ipairs(self.entities) do
		if e ~= self.entitydragging and not e.ignoreDrop and e:rect():contains(finn.mouse.x, finn.mouse.y) then
			table.insert(inside, e)
		end
	end
	local highest = self:getTopEntityInArray(inside)
	if highest ~= nil then
		highest:onDrop(self.entitydragging)
	end
end

function entitymanager:onDrag(mx, my)
	if self.previousclicked ~= nil and not self.previousclicked.ignoreDrag then 
		self.entitydragging = self.previousclicked
		self.previousclicked:onDrag(mx, my) 
	end
end

function entitymanager:onMouseMove(mx, my)
	if finn.mouse.button == finn.mouse.left and 
		finn.mouse.state == finn.pressed and
		self.lastClickedX ~= mx and self.lastClickedY ~= my then
		self:onDrag(mx, my)
	end
	local highest = self:getTopEntityAtMouse(mx, my, function(e) return e.ignoreHover end)
	if self.previoushover ~= nil then
		if not self.previoushover:rect():contains(mx, my) or highest ~= self.previoushover then
			self.previoushover:hoverExit()
			self.previoushover = nil
		end
	end
	if highest ~= nil then 
		highest:hoverOver() 
		self.previoushover = highest
	end
end

function entitymanager:onMousePressed(button)
	self.lastClickedX, self.lastClickedY = finn.mouse.x, finn.mouse.y
	local entitiesclicked = {}
	for i, e in ipairs(self.entities) do
		if e:rect():contains(finn.mouse.x, finn.mouse.y) then
			table.insert(entitiesclicked, e)
		end
	end
	for _, e in pairs(entitiesclicked) do
		if (self.previousclicked == nil or self.previousclicked:layer() < e:layer()) and 
				not e.ignoreClick then 
			self.previousclicked = e
		end
	end
	if self.previousclicked ~= nil then
		self.previousclicked:onClick(button)
	end
end

function entitymanager:onMouseReleased(button)
	if self.previousclicked ~= nil then
		self.previousclicked:onClickRelease(button)
		self.previousclicked = nil
	end
	if self.entitydragging ~= nil then
		self:onDrop()
		self.entitydragging = nil
	end
end