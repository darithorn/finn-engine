require "lib/class"
require "lib/entitymanager"
require "lib/vector2"
require "lib/rect"

class "entity"
-- entityManager : entitymanager | file : string | name : string | tag : string
function entity:entity(entityManager, file, name, tag)
	--[[
	self.name	-> name of entity
	self.tag	-> tag of entity
	self.sprite	-> the sprite used for drawing
	self.active	-> used by entitymanager to determine whether or not to render the entity
	]]
	if name then self.name = name else self.name = "" end
	if tag then self.tag = tag else self.tag = "" end
	if type(file) == "string" then 
		self.sprite = finn.sprite.createFromSVG(file)
		self.active = true
	elseif type(file) == "userdata" then
		self.sprite = file
		self.active = true
	else
		self.sprite = nil
		self.active = false
	end
	self.ignoreHover = false
	self.ignoreClick = false
	self.ignoreDrag = false
	self.ignoreDrop = false
	
	if self.sprite ~= nil then self._rect = rect(0, 0, finn.sprite.getWidth(self.sprite), finn.sprite.getHeight(self.sprite))
	else self._rect = rect(0, 0, 0, 0) end
	self._layer = entityManager:addEntity(self)
	self._entityManager = entityManager
end
-- Properties
function entity:position(x, y)
	if x == nil and y == nil then return vector2(self._rect.x, self._rect.y) end
	if x ~= nil then self._rect.x = x end
	if y ~= nil then self._rect.y = y end
end
function entity:positionX() return self._rect.x end
function entity:positionY() return self._rect.y end
function entity:rect(x, y, w, h)
	if x == nil and y == nil and w == nil and h == nil then return self._rect end
	if x ~= nil then self:position(x, nil) end
	if y ~= nil then self:position(nil, y) end
	if w ~= nil then self._rect.w = w end
	if h ~= nil then self._rect.h = h end
end
function entity:layer(l, sort)
	if l == nil then return self._layer end
	self._layer = l
	if sort ~= nil then self.em:sortByLayer() end
end
function entity:entityManager() return self._entityManager end

function entity:ignoreAll()
	self.ignoreHover = true
	self.ignoreClick = true
	self.ignoreDrag = true
	self.ignoreDrop = true
end
function entity:listenAll()
	self.ignoreHover = false
	self.ignoreClick = false
	self.ignoreDrag = false
	self.ignoreDrop = false
end

function entity:update(dt) end
function entity:render(x, y, offset) -- !! all 3 optional !!
	if x and y and offset == true then 
		finn.sprite.draw(self.sprite, self:positionX() + x, self:positionY() + y)
	elseif x and y then
		finn.sprite.draw(self.sprite, x, y)
	else
		finn.sprite.draw(self.sprite, self:positionX(), self:positionY())
	end
end

function entity:hoverOver() end
function entity:hoverExit() end
function entity:onClick(button) end
function entity:onClickRelease(button) end
function entity:onDrag(mx, my) end
function entity:onDrop(e) end

function entity:resize(w, h)
	finn.sprite.resize(self.sprite, w, h)
	self:rect(nil, nil, w, h)
end

function entity:resizeAspRat(scale)
	finn.sprite.resizeAspRat(self.sprite, scale)
	self:rect(nil, nil, self:rect().x * scale, self:rect().y * scale)
end
	
function entity:translate(dx, dy)
	self:position(self:positionX() + dx, self:positionY() + dy)
end

function entity:__tostring() return self.name end