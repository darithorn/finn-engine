Global Table: finn

Mouse Related Fields
--------------------
finn.mouse.state
	Returns the current mouse state.
		finn.pressed
		finn.released
	
finn.mouse.x
	Returns the mouse x position.
	
finn.mouse.y
	Returns the mouse y position.

finn.mouse.left	 	-> "mouseLeft"
finn.mouse.right	-> "mouseRight"
finn.mouse.middle 	-> "mouseMiddle"
	
Misc. Fields
------------
finn.none 		 -> "none"
finn.released 	 -> "released"
finn.pressed	 -> "pressed"
finn.shift		 -> "shift"
finn.ctrl		 -> "ctrl"
finn.alt		 -> "alt"
finn.normal		 -> "normal"
finn.italic		 -> "italic"
finn.oblique	 -> "oblique"
finn.bold		 -> "bold"
finn.screen.width
	Returns the screen width.
finn.screen.height
	Returns the screen height.

User Overloaded Functions
-------------------------
finn.update(dt)
	Called every frame. 
	dt is the delta time of the current frame.
	
finn.render()
	Called every frame after finn.update.
	
finn.quit()
	Called when a QUIT event is processed.
	1 frame will pass after finn.quit() is called.
	
finn.onMouseMove(mx, my)
	Called when the mouse is moved. 
	mx is mouse position x
	my is mouse position y
	
finn.onMousePressed(button)
	Called when a mouse button is pressed.
	button can be one of the following:
		finn.mouseLeft
		finn.mouseRight
		finn.mouseMiddle
		
finn.onMouseReleased(button)
	Called when a mouse button is released.
	button can be one of the following:
		finn.mouseLeft
		finn.mouseRight
		finn.mouseMiddle
		
finn.onKeyPressed(key, mod)
	Called when a keyboard key is pressed.
	key is a string referring to the key name.
	mod can be one of the following:
		finn.shift
		finn.ctrl
		finn.alt
		
finn.onKeyReleased(key, mod)
	Called when a keyboard key is released.
	key is a string referring to the key name.
	mod can be one of the following:
		finn.shift
		finn.ctrl
		finn.alt
		
Functions
---------
	Root
	----
		finn.root.limitFPS(fps : number)
			Sets the limit fps.
	
	Draw Utility
	------------
		finn.graphics.clearColor(r : number, g : number, b : number)
		finn.graphics.clearColor(r : number, g : number, b : number, normalized : boolean)
			Clears the color using values (r, g, b)
			Use normalized to specify if the values are normalized.
			
	Text
	----
		finn.font.create(family : string, size : number, slant : string, weight : string) -> finn_font
			Creates a new font.
			slant must be one of the following:
				finn.normal
				finn.italic
				finn.oblique
			weight must be one of the following:
				finn.normal
				finn.bold
			
		finn.font.getSize(font : finn_font) -> number
			Returns the font size.
			
		finn.font.set(font : finn_font)
			Sets the current font.
			
	Text
	----
		finn.text.setColor(r : number, g : number, b : number)
			Sets the text color. default -> black rgb(0, 0, 0)
			
		finn.text.draw(text : string, x : number, y : number)
			Draws text at the position (x, y) using the current font and color.
	
	Sprites
	-------
		finn.sprite.create(file : string) -> finn_sprite
			Creates a sprite from file.
		
		finn.sprite.createFromImage(img : finn_image) -> finn_sprite
			Creates a sprite from the given image.
			
		finn.sprite.draw(sprite : finn_sprite, x : number, y : number)
			Draws the sprite at position (x, y)
			
		finn.sprite.getFile(sprite : finn_sprite) -> string
			Returns the file that was loaded for the sprite's image.
			
		finn.sprite.getWidth(sprite : finn_sprite) -> number
			Returns the sprite's width
		
		finn.sprite.getHeight(sprite : finn_sprite) -> number
			Returns the sprite's height
			
		finn.sprite.setRegion(sprite : finn_sprite, x : number, y : number, w : number, h : number)
			Sets the sprite's texture region to rect (x, y, w, h)
			This resizes the sprite to fit the new rect.
			
		finn.sprite.resize(sprite : finn_sprite, w : number, h : number)
			Resizes the sprite to width -> w and height -> h.
			
	Image
	-----
		finn.image.create(file) -> finn_image
			Creates an image from the loaded file.