require "lib/class"

class "rect"

function rect:rect(x, y, w, h)
	self.x = x
	self.y = y
	self.w = w
	self.h = h
end

function rect:contains(x, y)
	if x and not y then -- contains rect
		local xmin = x.x
		local xmax = (xmin + x.w)
		local ymin = x.y
		local ymax = (ymin + x.h)
		return ((xmin > x and xmin < (self.x + self.width)) and (xmax > x and xmax < (self.x + self.width)) and
				(ymin > y and ymin < (self.y + sel.height)) and (ymax > y and ymax < (self.y + self.height)))
	elseif x and y then -- contains (x, y)
		return (self.x <= x and self.y <= y and
				(self.x + self.w) >= x and (self.y + self.h) >= y)
	end
	return false -- shouldn't need this but just in case
end

function rect:overlaps(r)
	return self.x < (r.x + r.w) and (self.x + self.w) > r.x 
			and self.y < (r.y + r.h) and (self.y + self.h) > r.y
end