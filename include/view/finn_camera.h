#ifndef __FINN_VIEW_CAMERA_H__
#define __FINN_VIEW_CAMERA_H__

typedef enum {
	FINN_PROJECTION_ORTHOGRAPHIC,
	FINN_PROJECTION_PERSPECTIVE
} FINN_PROJECTION_TYPE;

typedef struct {
	FINN_PROJECTION_TYPE type;
	float projection[16];
	float view[16];
} finn_camera;

extern finn_camera *g_finn_main_camera;

finn_camera *finn_camera_create_ortho(float width, float height, float near, float far);
void finn_camera_destroy(finn_camera *camera);

#endif
