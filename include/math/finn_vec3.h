#ifndef __FINN_MATH_VECTOR3_H__
#define __FINN_MATH_VECTOR3_H__
// TODO(darithorn): make these memory safe by using float[3]
void finn_vec3_print(float *vec);
void finn_vec3_up(float *vec);
void finn_vec3_right(float *vec);
void finn_vec3_forward(float *vec);
void finn_vec3_zero(float *vec);
void finn_vec3_one(float *vec);
float finn_vec3_dot(float *lhs, float *rhs);
float finn_vec3_mag(float *lhs);
void finn_vec3_normalize(float *result, float *vec);
void finn_vec3_cross(float *result, float *lhs, float *rhs);
void finn_vec3_mul_vec3(float *result, float *lhs, float *rhs);
void finn_vec3_mul_scalar(float *result, float *lhs, float scalar);
void finn_vec3_sub_vec3(float *result, float *lhs, float *rhs);
#endif
