#ifndef __FINN_MATH_VECTOR2_H__
#define __FINN_MATH_VECTOR2_H__
// TODO(darithorn): make these memory safe by using float[2]
void finn_vec2_one(float *result);
void finn_vec2_zero(float *result);
void finn_vec2_up(float *result);
void finn_vec2_right(float *result);
void finn_vec2_create(float *result, float x, float y);
float finn_vec2_dot(float *lhs, float *rhs);
float finn_vec2_magnitude(float *vec);
void finn_vec2_normalize(float *result, float *vec);
void finn_vec2_add_vec2(float *result, float *lhs, float *rhs);
void finn_vec2_sub_vec2(float *result, float *lhs, float *rhs);
void finn_vec2_mul(float *result, float *lhs, float scalar);
#endif
