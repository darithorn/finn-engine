#ifndef __FINN_MATH_MAT4_H__
#define __FINN_MATH_MAT4_H__

#define finn_mat4_element(x, y) (x * 4 + y)

// TODO(darithorn): make these memory safe by using float[16]
void finn_mat4_identity(float *mat);
void finn_mat4_translate(float *mat, float *vec);
void finn_mat4_translate_xyz(float *mat, float x, float y, float z);
void finn_mat4_scale(float *mat, float *vec);
void finn_mat4_scale_xyz(float *mat, float x, float y, float z);
void finn_mat4_rotate(float *mat, float *axis, float rad);
void finn_mat4_ortho(float *mat, float left, float right, float bottom, float top, float near, float far);
void finn_mat4_look_at(float *result, float *eye, float *at, float *up);
void finn_mat4_perspective(float *result, float fov, float aspect, float near, float far);
void finn_mat4_mul_mat4(float *result, float *lhs, float *rhs);

#endif
