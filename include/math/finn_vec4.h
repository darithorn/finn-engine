#ifndef __FINN_MATH_VECTOR4_H__
#define __FINN_MATH_VECTOR4_H__
// TODO(darithorn): make these memory safe by using float[4]
void finn_vec4_mul_vec4(float *result, float *lhs, float *rhs);
void finn_vec4_mul_mat4(float *result, float *vec, float *mat);

#endif
