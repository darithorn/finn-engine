#ifndef __FINN_INPUT_H__
#define __FINN_INPUT_H__

typedef enum {
	FINN_EVENT_NONE,
	FINN_QUIT,
	FINN_MOUSE_MOVE,
	FINN_MOUSE_PRESS,
	FINN_KEY_PRESS
} FINN_EVENT_TYPE;

typedef enum {
	FINN_MOUSE_LEFT,
	FINN_MOUSE_RIGHT,
	FINN_MOUSE_MIDDLE,
	FINN_MOUSE_NONE
} FINN_MOUSE_BUTTON;

typedef enum {
	FINN_PRESS_NONE,
	FINN_PRESS_UP,
	FINN_PRESS_DOWN
} FINN_BUTTON_PRESS;

typedef enum {
	FINN_MOD_NONE,
	FINN_MOD_SHIFT,
	FINN_MOD_CONTROL,
	FINN_MOD_ALT
} FINN_KEY_MOD;

typedef struct {
	FINN_EVENT_TYPE type;
	float mx, my; // mouse x - mouse y
	FINN_MOUSE_BUTTON mousebutton;
	FINN_BUTTON_PRESS mousepress;
		
	const char* keyname;
	FINN_BUTTON_PRESS keypress;
	FINN_KEY_MOD keymod;
} finn_input;
// Saves the input state for that frame
finn_input* g_input_state;

int finn_input_init();
void finn_input_free();
void finn_input_new_frame();

void finn_set_quit_event();

void finn_set_mouse_position(float mx, float my);
void finn_get_mouse_position(float* mx, float* my);

void finn_set_mouse_button(FINN_MOUSE_BUTTON button, FINN_BUTTON_PRESS press);
void finn_get_mouse_button(FINN_MOUSE_BUTTON* button, FINN_BUTTON_PRESS* press);

void finn_set_key_press(const char* keyname, FINN_KEY_MOD mod, FINN_BUTTON_PRESS press);
void finn_get_mouse_press(const char* keyname, FINN_KEY_MOD* mod, FINN_BUTTON_PRESS* press);

#endif
