#ifndef __FINN_EVENT_H__
#define __FINN_EVENT_H__
#include "finn_input.h"

struct finn_mouse_event {
	float mx, my;
	FINN_MOUSE_BUTTON button;
	FINN_BUTTON_PRESS state;
};
typedef struct finn_mouse_event finn_mouse_event;

struct finn_keyboard_event {
	const char* key;
	FINN_KEY_MOD mod;
	FINN_BUTTON_PRESS state;
};
typedef struct finn_keyboard_event finn_keyboard_event;

struct finn_event {
	FINN_EVENT_TYPE type;
	union {
		finn_mouse_event mouse;
		finn_keyboard_event keyboard;
	};
};
typedef struct finn_event finn_event;

void finn_events_clear();
void finn_add_mouse_event(FINN_EVENT_TYPE type, float mx, float my, FINN_MOUSE_BUTTON button, FINN_BUTTON_PRESS state); 
void finn_add_keyboard_event(FINN_EVENT_TYPE type, const char* keyname, FINN_KEY_MOD mod, FINN_BUTTON_PRESS state);
void finn_event_add(finn_event* event);
int finn_event_poll(finn_event* event);

#endif
