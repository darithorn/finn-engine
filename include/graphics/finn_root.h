#ifndef __FINN_WINDOW_H__
#define __FINN_WINDOW_H__
#include "finn_quad.h"
#include "finn_texture.h"

typedef struct {
	float width, height;
	float r, g, b;
	float pixel_x_ratio, pixel_y_ratio;
	int fps;
	float norm_fps;
	// finn_quad* quad;
	// finn_texture* texture;
} finn_root;

finn_root* g_root;

int finn_root_init(uint32_t width, uint32_t height);
void finn_root_destroy();
void finn_root_draw();
void finn_root_limit_fps(int fps);
float finn_root_get_width();
float finn_root_get_height();

#endif
