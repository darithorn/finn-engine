#ifndef __FINN_FONT_H__
#define __FINN_FONT_H__
#ifdef __LIBCAIRO__
#include <cairo/cairo.h>
#endif

typedef enum {
	FINN_SLANT_NORMAL,
	FINN_SLANT_ITALIC,
	FINN_SLANT_OBLIQUE
} FINN_FONT_SLANT;

typedef enum {
	FINN_WEIGHT_NORMAL,
	FINN_WEIGHT_BOLD
} FINN_FONT_WEIGHT;

typedef struct {
	const char* face;
	double size;
	#ifdef __LIBCAIRO__
	cairo_font_slant_t slant;
	cairo_font_weight_t weight;
	#endif
} finn_font;

finn_font* finn_font_create(const char* face_family, double size, FINN_FONT_SLANT slant, FINN_FONT_WEIGHT weight);
void finn_font_destroy(finn_font* font);

#endif
