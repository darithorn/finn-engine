#ifndef __QUAD_H__
#define __QUAD_H__
#ifdef _WIN32
#include <zed_gl.h>
#else
#include <GL/gl.h>
#endif

typedef enum {
	BOTTOM_LEFT,
	BOTTOM_RIGHT,
	TOP_LEFT,
	TOP_RIGHT
} QUAD_CORNER;

typedef enum {
	GL_2_X,
	GL_3_X
} GL_VERSION_X;

typedef struct {
	GLfloat positions[8];
	GLfloat tex_coords[8];
	float x_offset, y_offset;
	float width, height;
} finn_quad;

finn_quad* finn_quad_create(uint32_t root_width, uint32_t root_height, uint32_t width, uint32_t height);

void finn_quad_destroy(finn_quad* quad);

void finn_quad_draw(finn_quad* quad, float x, float y, GL_VERSION_X gl_version, int is_root);

int finn_quad_set_corner_position(finn_quad* quad, QUAD_CORNER corner, GLfloat x, GLfloat y);

int finn_quad_set_corner_tex_coord(finn_quad* quad, QUAD_CORNER corner, GLfloat u,  GLfloat v);

int finn_quad_resize(finn_quad* quad, uint32_t width, uint32_t height);

void finn_quad_move_by_pixel(finn_quad* quad, float pixel_x, float pixel_y, int pixel_x_amount, int pixel_y_amount);

#endif
