#ifndef __FINN_SPRITE_H__
#define __FINN_SPRITE_H__
#include "finn_texture.h"
#include "finn_quad.h"
#include "finn_image.h"

typedef struct {
	finn_texture* texture;
	finn_quad* quad;
	finn_image* img;
	#ifdef __LIBCAIRO__
	cairo_matrix_t scale;
	#endif
} finn_sprite;

finn_sprite* finn_sprite_create_from_svg(const char* file);
finn_sprite* finn_sprite_create_from_png(const char* file);
finn_sprite* finn_sprite_create_from_image(finn_image* img);
void finn_sprite_destroy(finn_sprite* sprite);
void finn_sprite_draw(finn_sprite* sprite, float x, float y);
void finn_sprite_resize(finn_sprite* sprite, float w, float h);
void finn_sprite_resize_aspect_ratio(finn_sprite* sprite, float scale);
void finn_sprite_set_region(finn_sprite* sprite, float x, float y, float w, float h);

#endif
