#ifndef __FINN_DRAW_UTILITY_H__
#define __FINN_DRAW_UTILITY_H__
#include "finn_texture.h"
#include "finn_color.h"
#include "finn_image.h"
#include "finn_font.h"

void finn_texture_set_rgb(finn_texture* texture, float r, float g, float b);
void finn_texture_set_rgba(finn_texture* texture, float r, float g, float b, float a);

void finn_texture_set_position(finn_texture* texture, float x, float y);

void finn_texture_fill_background_color(finn_texture* texture, finn_color color);
void finn_texture_fill_background_rgb(finn_texture* texture, float r, float g, float b);
void finn_texture_fill_background_normalized_rgb(finn_texture* texture, float r, float g, float b);

void finn_texture_draw_image(finn_texture* texture, finn_image* image, float x, float y);

void finn_root_set_font(finn_font* font);
void finn_root_set_text_rgb(float r, float g, float b);
void finn_root_draw_text_with_font(finn_font* font, const char* text, double x, double y);
void finn_root_draw_text(const char* text, double x, double y);

#endif
