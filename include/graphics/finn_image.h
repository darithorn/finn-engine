#ifndef __FINN_IMAGE_H__
#define __FINN_IMAGE_H__
#ifdef __LIBCAIRO__
#include <cairo/cairo.h>
#endif
#ifdef __LIBRSVG__
#include <librsvg-2.0/librsvg/rsvg.h>
#endif

typedef enum {
	IMAGE_TYPE_SVG,
	IMAGE_TYPE_PNG,
	IMAGE_TYPE_JPG
} IMAGE_TYPE;

typedef struct {
	const char* file;
	IMAGE_TYPE image_type;
	int width, height;
	// if imagetype == SVG handle will be RsvgHandle*
	// otherwise cairo_surface_t*
	void* handle;
} finn_image;

finn_image* finn_image_create_from_svg(const char* filename);
finn_image* finn_image_create_from_png(const char* filename);
void finn_image_destroy(finn_image* img);

#endif
