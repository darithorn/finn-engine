#ifndef __FINN_COLOR_H__
#define __FINN_COLOR_H__

struct finn_color {
	int r, g, b;
};
typedef struct finn_color finn_color;

void finn_color_get_normalized(finn_color color, float* red, float* green, float* blue);

#endif
