#ifndef __FINN_TEXTURE_H__
#define __FINN_TEXTURE_H__
#ifdef _WIN32
#include <zed_gl.h>
#else
#include <GL/gl.h>
#endif
#ifdef __LIBCAIRO__
#include <cairo/cairo.h>
#endif

typedef struct {
	GLuint id;
	float width, height;
	#ifdef __LIBCAIRO__
	cairo_t* context;
	cairo_surface_t* surface;
	#endif
	unsigned char* data;
} finn_texture;

finn_texture* finn_texture_create(float width, float height);

void finn_texture_destroy(finn_texture* texture);

int finn_texture_resize(finn_texture* texture, float width, float height);

void finn_texture_bind(finn_texture* texture);
#endif
