#ifndef __FINN_LUA_H__
#define __FINN_LUA_H__
#include <lua5.2/lua.h>
#include <lua5.2/lauxlib.h>
#include <lua5.2/lualib.h>

int finn_lua_load(const char* file);
void finn_lua_cleanup();
void finn_lua_send_quit();
void finn_lua_update(float dt);

#endif
