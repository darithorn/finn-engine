#ifndef __FINN_FONT_LUA_H__
#define __FINN_FONT_LUA_H__
#include <lua5.2/lua.h>

int finn_lua_font_create(lua_State* l);
int finn_lua_font_set_size(lua_State* l);
int finn_lua_font_get_size(lua_State* l);

#endif
