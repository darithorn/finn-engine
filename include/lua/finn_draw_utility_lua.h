#ifndef __FINN_DRAW_UTILITY_LUA_H__
#define __FINN_DRAW_UTILITY_LUA_H__
#include <lua5.2/lua.h>

// finn.fillBackgroundColor(r, g, b, n)
int finn_lua_fill_background_color(lua_State* l);

// finn.setFont(font)
int finn_lua_set_font(lua_State* l);
int finn_lua_set_text_color(lua_State* l);
int finn_lua_draw_text(lua_State* l);

#endif
