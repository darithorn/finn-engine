#ifndef __FINN_LUA_UTILITY_H__
#define __FINN_LUA_UTILITY_H__
#include <lua5.2/lua.h>

void finn_lua_clear_stack(lua_State* l);
void finn_lua_clear_stack_offset(lua_State* l, int offset);

#endif
