#ifndef __FINN_IMAGE_LUA_H__
#define __FINN_IMAGE_LUA_H__
#include <lua5.2/lua.h>

int finn_lua_image_create_from_png(lua_State* l);
int finn_lua_image_create_from_svg(lua_State* l);
int finn_lua_image_get_width(lua_State* l);
int finn_lua_image_get_height(lua_State* l);

#endif
