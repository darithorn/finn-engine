#ifndef __FINN_ROOT_LUA_H__
#define __FINN_ROOT_LUA_H__
#include <lua5.2/lua.h>

// finn.setWindowSize(w, h)
// int finn_setWindowSizeLua(lua_State* lua);
// finn.limitFPS(fps)
int finn_lua_root_limit_fps(lua_State* lua);

#endif
