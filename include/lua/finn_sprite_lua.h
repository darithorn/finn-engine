#ifndef __FINN_SPRITE_LUA_H__
#define __FINN_SPRITE_LUA_H__
#include <lua5.2/lua.h>

int finn_lua_sprite_create_from_png(lua_State* l);
int finn_lua_sprite_create_from_svg(lua_State* l);
int finn_lua_sprite_create_from_image(lua_State* l);
int finn_lua_sprite_draw(lua_State* l);

int finn_lua_sprite_get_file(lua_State* l);
int finn_lua_sprite_get_width(lua_State* l);
int finn_lua_sprite_get_height(lua_State* l);

int finn_lua_sprite_set_region(lua_State* l);
int finn_lua_sprite_resize(lua_State* l);
int finn_lua_sprite_resize_aspect_ratio(lua_State* l);

#endif
