#ifndef __FINN_ERROR_H__
#define __FINN_ERROR_H__

int finn_errors_init();
void finn_errors_free();

int finn_has_error();
// Gets the most recent error
const char* finn_error_get();

void finn_error_add(const char* message);

#endif
