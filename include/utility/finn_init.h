#ifndef __FINN_INIT_H__
#define __FINN_INIT_H__

typedef enum {
	FINN_INIT_EVERYTHING
} FINN_INIT_FLAGS;

int finn_init(FINN_INIT_FLAGS flags);

void finn_quit();

#endif
