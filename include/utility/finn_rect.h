#ifndef __FINN_RECT_H__
#define __FINN_RECT_H__
#include <stdint.h>

struct finn_rect {
	int x, y;
	uint32_t width, height;
};
typedef struct finn_rect finn_rect;

float* finn_rect_get_corners(finn_rect rect);

#endif
