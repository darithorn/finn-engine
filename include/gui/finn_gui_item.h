#ifndef __FINN_GUI_ITEM_H__
#define __FINN_GUI_ITEM_H__
#include "gui/finn_gui_type.h"
#include "utility/finn_rect.h"

typedef struct finn_gui_item_s {
	FINN_GUI_TYPE type;
	finn_rect bounds;
	struct finn_gui_item_s** children;
	int finn_gui_children_length;
	int finn_gui_children_max;
	/*union {
		//finn_gui_button* button;
		//finn_gui_label* label;
	};*/
} finn_gui_item;

finn_gui_item finn_gui_item_create(FINN_GUI_TYPE type);

#endif
