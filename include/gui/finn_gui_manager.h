#ifndef __FINN_GUI_MANAGER_H__
#define __FINN_GUI_MANAGER_H__
#include "gui/finn_gui_item.h"

/// The amount of items allocated per step
#define FINN_GUI_ITEM_STEP 10

typedef struct {
	// The gui item with current focus, can be NULL
	finn_gui_item* focus_item;
	finn_gui_item** gui_items;
	int gui_items_length;
	int gui_items_max;
} finn_gui_manager;

extern finn_gui_manager* g_gui_manager;

int finn_gui_manager_init();
void finn_gui_manager_free();
int finn_gui_manager_add_item(finn_gui_item* item);

#endif
