#ifndef __FINN_PROGRAM_H__
#define __FINN_PROGRAM_H__
#include <sys/types.h>
#ifdef _WIN32
#include <SDL_video.h>
#include <SDL_events.h>
#include <nanovg.h>
#else
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_events.h>
#endif

#include "utility/finn_init.h"

typedef struct {
	int32_t width, height;
	uint32_t last_time, current_time, delta_time;
	
	SDL_Window* window;
	SDL_Event event;
	SDL_GLContext context;
#ifdef _WIN32
	struct NVGcontext *nvg_context;
#endif
} finn_program;

finn_program* finn_program_create(const char* title, int32_t width, int32_t height);
finn_program* finn_program_create_init(const char* title, int32_t width, int32_t height, FINN_INIT_FLAGS finn_flags, uint32_t sdl_flags);
void finn_program_destroy(finn_program* program);
void finn_program_destroy_exit(finn_program* program);
void finn_program_clear_background();
void finn_program_swap_buffer(finn_program* program);
void finn_program_update_input(finn_program* program);

#endif
